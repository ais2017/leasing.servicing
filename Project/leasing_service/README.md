Описание
========
+ ОС - Ubuntu 16.04
+ БД - sqlite3


Установка нужных пакетов
========================
    sudo apt-get install python3 python3-pip python-django python-django-common
    sudo pip3 install pipenv

    pipenv install --python=python3.6 --dev
    pipenv run django-admin startproject leasing_service
    cd leasing_service


Допуск к бд
===========
        docker-compose up db
        docker-compose exec db bash
        psql -U leasing_service


Начало работы
=============
1. Запуск проекта:

        docker-compose up db
        pipenv run python manage.py makemigrations
        pipenv run python manage.py migrate
        pipenv run python manage.py runserver


Создание пользователя
=====================
        python manage.py createsuperuser
        admin -> admin

2. Сервис будет доступен локально по адресу:

        http://127.0.0.1:8000/


Запуск тестов
-------------

    pipenv run py.test


РАСШИРЕНИЕ 1:
Интерфейс к системе (для инженера и бригадира) - WEB интерфейс - SQL (любая реляционная не inMemory БД)
