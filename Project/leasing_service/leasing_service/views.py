# coding=utf-8
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import StaticHTMLRenderer
from rest_framework.response import Response

from django.shortcuts import redirect

# источники данных
from leasing_service.api_gateways.personnel_accounting_system_gateway import PersonnelAccountingSystemGateway
personnel_gateway = PersonnelAccountingSystemGateway()


"""
Базовая логика:
    Есть страница авторизации:
        из неё в зависимости от прав можно попасть в раздел для инженера или в раздел для руководителя
            где присутствуют соответствующие кнопки для соответствующих списков

    Методы:
        1. Запрос авторизационных данных
            Сверка их через систему учета персонала (заглушка)
        2. Получить список заявок на бригаде
        3. Просмотр заявки (только чтение -> просмотр списка тестов и конкретного теста (с возможностью редактировани))
            (кнопки назад, сохранить, завершить обслуживание, подтверждить корректность)
"""


@api_view(['GET', 'POST'])
@renderer_classes((StaticHTMLRenderer,))
def auth_view(request):
    """view страницы аутентификации"""
    # список ошибок
    errors = []

    # сначала проверим сессию (если пользователь уже аутенцифицирован перенаправить его на нужную ему страницу)
    if request.session.get('uid'):
        return redirect('/')  # Перенаправление на страницу для дальнейшей работы пользователя

    # если получили POST данные (с логином и паролем), анализируем их и:
    #   1. возвращаем форму с ошибкой аутентификации
    #   2. ставим сессию и отправляем на соответствующую правам пользователя форму
    if request.POST.get('username'):
        username = request.POST['username']
        password = request.POST['password']
        user = personnel_gateway.authentificate(request, username=username, password=password)
        if user is not None:
            request.session['uid'] = user.get('uid')
            request.session['username'] = user.get('username')
            return redirect('/')  # Перенаправление на страницу для дальнейшей работы пользователя
        else:
            errors.append('Некорректный login или password')  # Добавляем сообщения об ошибке

    # итоговое сообщение об ошибках
    error_msg = ''
    if errors:
        error_msg = 'Errors:<br/>' + '<br/>'.join(errors)

    # Ответ (начальный и в случае ошибки)
    return Response("""
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Аутентификация</title>
    </head>
    <body>
        <h1>Аутентификация</h1>
        <div>
            <form action="/auth/" method="post">
                <input name="username" type="text" placeholder="enter login" required />
                <br/><br/>
                <input name="password" type="password" placeholder="enter password" required />
                <br/><br/>
                <input type="submit" value="Save" />
            </form>
        </div>
        {error_msg}
    </body>
    </html>""".format(error_msg=error_msg), status.HTTP_200_OK)


def logout_view(request):
    request.session.clear()
    return redirect('/auth/')  # Перенаправление на страницу аутентификации


@api_view()
@renderer_classes((StaticHTMLRenderer,))
def engineer_main_view(request):
    """view основной страницы для инженера"""

    # сначала проверим сессию (если не установлена отправим на страницу аутентификации)
    if not request.session.get('uid'):
        return redirect('/auth/')  # Перенаправление на страницу аутентификации

    session_data = "uid: {uid}</br>username: {username}</br>".format(uid=request.session.get('uid'),
                                                                     username=request.session.get('username'))

    return Response("""
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Главное меню</title>
    </head>
    <body>
        <h1>Главное меню</h1>
        <div>
            <a href='/logout/'>logout</a>
            <br/>
            <a href='/maintenance-requests/'>maintenance requests</a>
            <br/><br/>
            Session data:
            <br/>
            {session}
        </div>
    </body>
    </html>""".format(session=session_data), status.HTTP_200_OK)


@api_view()
@renderer_classes((StaticHTMLRenderer,))
def maintenance_requests_view(request):
    """view страницы для просмотра списка заявок"""

    # сначала проверим сессию (если не установлена отправим на страницу аутентификации)
    if not request.session.get('uid'):
        return redirect('/auth/')  # Перенаправление на страницу аутентификации

    return Response("""
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Список заявок на ТО</title>
    </head>
    <body>
        <h1>Список заявок на ТО</h1>
        <div>
            <a href='/'>main engineer menu</a>
            <br/>
            <a href='/maintenance-requests/tests-list/'>test list</a>
        </div>
    </body>
    </html>""", status.HTTP_200_OK)


@api_view()
@renderer_classes((StaticHTMLRenderer,))
def maintenance_requests_tests_list_view(request):
    """view страницы для просмотра списка тестов в заявке"""

    # сначала проверим сессию (если не установлена отправим на страницу аутентификации)
    if not request.session.get('uid'):
        return redirect('/auth/')  # Перенаправление на страницу аутентификации

    return Response("""
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Список тестов в заявке на ТО №...</title>
    </head>
    <body>
        <h1>Список тестов в заявке на ТО №...</h1>
        <div>
            <a href='/'>main engineer menu</a>
        </div>
    </body>
    </html>""", status.HTTP_200_OK)
