# coding=utf-8
"""
Данный модуль содержит методы реализующие бизнес логику текущей системы
"""

import random  # Рандомизация
from datetime import datetime  # Время
# Подключение модулей для использования явной типизации
from typing import List, Dict

# Подключение модулей для использования явной типизации
from leasing_service.business_classes.personnel_accounting_system_classes import Employee, Brigade
from leasing_service.business_classes.system_classes import MaintenanceRequest


# Общие методы модуля
def _get_and_check_maintenance_request_by_uid(system_gateway, employee: Employee, uid: int) -> MaintenanceRequest:
    """Функция для получения заявки на ТО по идентификатору с валидацией допустимости выдачи"""
    """
    Ход работы:
    1. Получение заявки по её номеру из шлюза системы
    2. Если заявка закрыта - ошибка

    3. Если у заявки завершено ТО
    3.1. Если сотрудник не является бригадиром бригады на которую назначена заявка - ошибка

    4. Иначе если ТО не завершено
    4.1. Если сотрудник не является членом бригады на которую назначена заявка - ошибка

    5. Вернуть заявку на ТО
    """
    # Получение заявки по её номеру из шлюза системы
    maintenance_request = system_gateway.get_maintenance_request_by_uid(uid)

    # Если заявка закрыта - ошибка
    if maintenance_request.get_closing_date():
        raise ValueError("Заявка уже закрыта")

    if maintenance_request.get_date_of_completion_of_service():  # Если у заявки завершено ТО
        # Если сотрудник не является бригадиром бригады на которую назначена заявка - ошибка
        if maintenance_request.get_brigade().get_brigadier() != employee:
            raise ValueError("Работа с заявкой возможна только для бригадира назначенной бригады")
    else:  # Иначе если ТО не завершено
        # Если сотрудник не является членом бригады на которую назначена заявка - ошибка
        if not maintenance_request.get_brigade().find_employee_by_uid(employee.get_uid()):
            raise ValueError("Работа с заявкой возможна только для сотрудника назначенной бригады")

    return maintenance_request


def _select_brigade(brigades_workload: Dict) -> Brigade:
    """
    Случайным образом выбирает одну из самых незагруженных бригад
    :param brigades_workload: загруженность бригад (бригада - количество заявок на ней)
    :return brigade: одна из бригад с минимальной загруженностью
    """

    """
    Ход работы:
    1. Получение списка бригад с одинаково минимальным количеством заявок
    2. Установка нового значения зерну рандомизатора и получение индекса случайной бригады
    3. Получение бригады из списка и удаление её из неё для переноса в список более высокой нагрузки
    4. Если минимальный список опустел удалить его
    5. Возврат бригады
    """
    # Получение списка бригад с одинаково минимальным количеством заявок
    min_key = sorted(brigades_workload)[0]
    brigades_list = brigades_workload[min_key]
    # Установка нового значения зерну рандомизатора и получение индекса случайной бригады
    random.seed(datetime.now())
    brigade_index = random.randint(0, len(brigades_list) - 1)
    # Получение бригады из списка и удаление её из неё для переноса в список с более высокой нагрузки
    brigade = brigades_workload[min_key].pop(brigade_index)
    if not brigades_workload.get(min_key + 1):  # если такого ключа еще нет, создаем (ключ: пустой список)
        brigades_workload[min_key + 1] = []
    brigades_workload[min_key + 1].append(brigade)
    # Если минимальный список опустел удалить его
    if not len(brigades_workload[min_key]):
        brigades_workload.pop(min_key, '')
    # Возврат бригады
    return brigade


# Таймер
def create_tasks(system_gateway, equipment_gateway, personnel_gateway) -> None:
    """Создание заявок"""

    """
    Ход работы:
    1. Получение списка оборудования из системы учета оборудования через шлюз оборудования
    2. Получение списка бригад из системы учета персонала через шлюз персонала
    3. Формирование заявок и распределение из по бригадам с отправкой в шлюз системы
    3.1. Формируем начальную загруженность бригад (количество заявок: список бригад)
    3.2. Для каждого оборудования
    3.2.1. Находим случайным образом одну из самых незагруженных бригад
    3.2.2. Формируем заявку указав текущее оборудование и выбранную бригаду
    """
    # Получение списка оборудования из шлюза оборудования
    equipment_list = equipment_gateway.get_equipments()
    # Получение списка бригад из шлюза персонала
    brigades_list = personnel_gateway.get_brigades()

    # Формирование заявок и распределение их по бригадам
    # Формируем начальную загруженность бригад (количество заявок: список бригад)
    brigades_workload = {0: brigades_list}  # загруженность бригад

    # Для каждого оборудования
    for equipment in equipment_list:
        # Находим случайным образом одну из самых незагруженных бригад
        brigade = _select_brigade(brigades_workload)
        # Формируем заявку указав текущее оборудование и выбранную бригаду
        system_gateway.add_maintenance_request(equipment, brigade)


# Инженер бригады (в том числе бригадир)
def get_requests_list(system_gateway, personnel_gateway, employee_uid: int) -> List:
    """Предоставить список заявок"""
    # Инженер бригады только заявки с не завершенным ТО
    # Бригадиру о всех заявках на его бригаде

    """
    Ход работы:
    0. Получение информации о сотруднике по его uid
    1. Получение списка заявок из шлюза системы

    2. Для каждой заявки
    2.1. Если у заявки не завершено ТО
    2.1.1. Если сотрудник входит в бригаду у заявки то добавить заявку в список выдачи
    2.2. Если у заявки завершено ТО
    2.2.1. Если сотрудник бригадир бригады у заявки то добавить заявку в список выдачи
    2.3. Если заявка закрыта - пропустить её

    3. Отдать список заявок
    """
    # Получение информации о сотруднике по его uid
    employee = personnel_gateway.get_employee_by_uid(employee_uid)

    # Получение списка заявок из шлюза системы
    maintenance_requests_list = system_gateway.get_maintenance_requests()

    # Для каждой заявки
    employee_maintenance_requests_list = []  # список заявок сотрудника
    for maintenance_request in maintenance_requests_list:
        if not maintenance_request.get_date_of_completion_of_service():  # Если у заявки не завершено ТО
            # Если сотрудник входит в бригаду у заявки то добавить заявку в список выдачи
            if maintenance_request.get_brigade().find_employee_by_uid(employee.get_uid()):
                employee_maintenance_requests_list.append(maintenance_request)
        elif not maintenance_request.get_closing_date():  # Если у заявки завершено ТО и заявка не закрыта
            # Если сотрудник бригадир бригады у заявки то добавить заявку в список выдачи
            if maintenance_request.get_brigade().get_brigadier() == employee:
                employee_maintenance_requests_list.append(maintenance_request)

    # сформировать список словарей данных из собранных заявок
    employee_mr_list = []
    for maintenance_request in employee_maintenance_requests_list:
        employee_mr_list.append({
            'MR_uid': maintenance_request.get_uid(),
            'MR_type': maintenance_request.get_maintenance_type(),
            'MR_date_of_completion_of_service': maintenance_request.get_date_of_completion_of_service(),
            'MR_closing_date': maintenance_request.get_closing_date(),
            'MR_equipment_uid': maintenance_request.get_equipment().get_uid(),
            'MR_equipment_commissioning_date': maintenance_request.get_equipment().get_commissioning_date(),
            'MR_brigade_uid': maintenance_request.get_brigade().get_uid(),
        })

    # Отдать данные
    return employee_mr_list


def get_request_tests_list(system_gateway, personnel_gateway, employee_uid: int, uid: int) -> List:
    """Предоставить список тестов в заявке для просмотра"""

    """
    Ход работы:
    0. Получение информации о сотруднике по его uid
    1. Получение заявки с валидацией
    2. Возврат списка тестов по заявке
    """
    # Получение информации о сотруднике по его uid
    employee = personnel_gateway.get_employee_by_uid(employee_uid)

    # Получение заявки с валидацией
    maintenance_request = _get_and_check_maintenance_request_by_uid(system_gateway, employee, uid)

    # сформировать список словарей с данными о тестах
    equipment_tests_list = []
    for equipment_test in maintenance_request.get_equipment().get_all_tests():
        equipment_tests_list.append({
            'ET_name': equipment_test.get_name(),
            'ET_type': equipment_test.get_maintenance_type(),
        })

    # Возврат списка тестов по заявке
    return equipment_tests_list


def get_request_test_results_list(system_gateway, personnel_gateway, employee_uid: int, uid: int) -> List:
    """Предоставить список результатов тестов в заявке для просмотра"""

    """
    Ход работы:
    0. Получение информации о сотруднике по его uid
    1. Получение заявки с валидацией
    2. Возврат списка результатов тестов по заявке
    """
    # Получение информации о сотруднике по его uid
    employee = personnel_gateway.get_employee_by_uid(employee_uid)

    # Получение заявки с валидацией
    maintenance_request = _get_and_check_maintenance_request_by_uid(system_gateway, employee, uid)

    # сформировать список словарей с данными о тестах
    mr_test_results_list = []
    for mr_test_result in maintenance_request.get_test_result_list():
        employee_updated_data_uid = 'None'
        employee_updated_data_name = 'None'
        if mr_test_result.get_employee_updated_data():
            employee_updated_data_uid = mr_test_result.get_employee_updated_data().get_uid()
            employee_updated_data_name = mr_test_result.get_employee_updated_data().get_name()

        mr_test_results_list.append({
            'MR_TR_name': mr_test_result.get_name(),
            'MR_TR_type': mr_test_result.get_equipment_test().get_maintenance_type(),
            'MR_TR_data': mr_test_result.get_data(),
            'MR_TR_employee_entered_data_uid': mr_test_result.get_employee_entered_data().get_uid(),
            'MR_TR_employee_entered_data_name': mr_test_result.get_employee_entered_data().get_name(),
            'MR_TR_employee_updated_data_uid': employee_updated_data_uid,
            'MR_TR_employee_updated_data_name': employee_updated_data_name,
            'MR_TR_update_reason': mr_test_result.get_update_reason(),
            'MR_TR_state': mr_test_result.get_state(),
        })

    # Возврат списка результатов тестов по заявке
    return mr_test_results_list


def add_request_test_results(system_gateway, personnel_gateway, employee_uid: int, uid: int,
                             equipment_test_name: str, data: str, state: bool) -> None:
    """Внесение результатов теста"""
    # Инженер бригады только до завершения ТО
    # Бригадир только до закрытия заявки

    """
    Ход работы:
    0. Получение информации о сотруднике по его uid
    1. Получение заявки с валидацией
    2. Получение теста оборудования по имени
    3. Добавить к заявке результат теста
    4. Если при добавлении вернулось False или None - ошибка
    """
    # Получение информации о сотруднике по его uid
    employee = personnel_gateway.get_employee_by_uid(employee_uid)

    # Получение заявки с валидацией
    maintenance_request = _get_and_check_maintenance_request_by_uid(system_gateway, employee, uid)

    # получить тест оборудования по имени
    equipment_test = maintenance_request.get_equipment().get_test_by_name(equipment_test_name)
    if not equipment_test:
        raise ValueError("Такого теста нет у оборудования")

    # Добавить к заявке результат теста, при неудаче поолучить ошибку
    system_gateway.add_maintenance_request_test_result_by_uid(uid, equipment_test, data, employee, state)


def update_request_test_results(system_gateway, personnel_gateway, employee_uid: int, uid: int,
                                equipment_test_name: str, data: str, update_reason: str, state: bool = None) -> None:
    """Корректировака результатов тестов"""
    # Инженер бригады только до завершения ТО
    # Бригадир только до закрытия заявки

    """
    Ход работы:
    0. Получение информации о сотруднике по его uid
    1. Получение заявки с валидацией
    2. Получение теста оборудования по имени
    3. Обновить в заявке результат теста
    4. Если при обновлении вернулось False или None - ошибка
    """
    # Получение информации о сотруднике по его uid
    employee = personnel_gateway.get_employee_by_uid(employee_uid)

    # Получение заявки с валидацией
    maintenance_request = _get_and_check_maintenance_request_by_uid(system_gateway, employee, uid)

    # получить тест оборудования по имени
    equipment_test = maintenance_request.get_equipment().get_test_by_name(equipment_test_name)
    if not equipment_test:
        raise ValueError("Такого теста нет у оборудования")

    # Обновить в заявке результат теста, при неудаче поолучить ошибку
    system_gateway.update_maintenance_request_test_result_by_uid(uid, equipment_test, data, employee, update_reason, state)


def service_complete(system_gateway, personnel_gateway, employee_uid: int, uid: int) -> None:
    """Завершение технического обслуживания"""

    """
    Ход работы:
    0. Получение информации о сотруднике по его uid
    1. Получение заявки по её номеру из шлюза системы
    2. Если сотрудник не является членом бригады на которую назначена заявка - ошибка
    3. Завершить ТО у заявки
    4. Если при завершении вернулось False - ошибка
    """
    # Получение информации о сотруднике по его uid
    employee = personnel_gateway.get_employee_by_uid(employee_uid)

    # Получение заявки по её номеру из шлюза системы
    maintenance_request = system_gateway.get_maintenance_request_by_uid(uid)

    # Если сотрудник не является членом бригады на которую назначена заявка - ошибка
    if not maintenance_request.get_brigade().find_employee_by_uid(employee.get_uid()):
        raise ValueError("Работа с заявкой возможна только для сотрудника назначенной бригады")

    # Завершить ТО у заявки, при неудаче поолучить ошибку
    system_gateway.maintenance_request_service_complete_by_uid(uid)


# Руководитель (бригадир)
def service_data_correct(system_gateway, personnel_gateway, employee_uid: int, uid: int) -> None:
    """Подтверждение корректности и закрытие заявки"""
    # Только бригадиру и только о заявках на его бригаде

    """
    Ход работы:
    0. Получение информации о сотруднике по его uid
    1. Получение заявки по её номеру из шлюза системы
    2. Если сотрудник не является бригадиром бригады на которую назначена заявка - ошибка
    3. Подтверждение корректности заявки
    4. Если при подтверждении корректности вернулось False - ошибка
    """
    # Получение информации о сотруднике по его uid
    employee = personnel_gateway.get_employee_by_uid(employee_uid)

    # Получение заявки по её номеру из шлюза системы
    maintenance_request = system_gateway.get_maintenance_request_by_uid(uid)

    # Если сотрудник не является бригадиром бригады на которую назначена заявка - ошибка
    if maintenance_request.get_brigade().get_brigadier() != employee:
        raise ValueError("Работа с заявкой возможна только для бригадира назначенной бригады")

    # Подтверждение корректности заявки, при неудаче поолучить ошибку
    system_gateway.maintenance_request_service_data_correct_by_uid(uid)


# Внешнее API
def check_equipment_state(system_gateway, equipment_gateway, equipment_uid: int) -> None:
    """Функция которая проверяет состояние оборудования по последднему ТО для него"""

    """
    Ход работы:
    0. Получение информации об оборудовании по его uid
    1. Получение заявок из шлюза системы
    2. Получение последей заявки для оборудования из шлюза системы
    3. Проверка тестов оборудования на успешность
    3.1. Если хоть один результат теста не успешен - ошибка
    """
    # Получение информации об оборудовании по его uid
    equipment = equipment_gateway.get_equipment_by_uid(equipment_uid)

    # Получение последей заявки для оборудования из шлюза системы
    last_equipment_maintenance_request = system_gateway.get_last_equipment_maintenance_request(equipment)

    # Проверка тестов оборудования на успешность
    for test in last_equipment_maintenance_request.get_test_result_list():
        # Если хоть один результат теста не успешен - ошибка
        if not test.get_state():
            raise ValueError("Оборудование непригодно к эксплуатации")
