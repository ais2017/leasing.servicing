# coding=utf-8
from datetime import datetime, timedelta
from unittest import mock

import pytest

# Экземпляры шлюзов для тестов
from leasing_service.api_gateways.equipment_accounting_system_gateway import EquipmentAccountingSystemGateway
from leasing_service.api_gateways.personnel_accounting_system_gateway import PersonnelAccountingSystemGateway
from leasing_service.api_gateways.system_gateway import SystemGatewayInMemory
# Дополнительные классы
from leasing_service.business_classes.system_classes import MaintenanceRequest, RequestTestResult
from leasing_service.business_classes.personnel_accounting_system_classes import Employee, Brigade
from leasing_service.business_classes.equipment_accounting_system_classes \
    import MaintenanceType, EquipmentTest, Equipment
# Тестируемые методы реализующие бизнес-логику
from leasing_service.business_logic import system_methods

"""
Чек-лист методов реализующих бизнес логику текущей системы:
    Метод "Получения заявки" (внутренний метод):
        + 1. Успешный возврат заявки
        + 2. Заявка уже закрыта
        + 3. ТО завершено и сотрудник не бригадир бригады
        + 4. ТО не завершено и сотрудник не входит в бригаду

    Метод "Выбор самой ненагруженной бригады" (внутренний метод):
        + 1. Успешное распределение бригад

    Метод "Создание заявок":
        + 1. Успешное создание заявок

    Метод "Предоставить список заявок":
        + 1. Содержимое списка соответствует ожиданиям

    Метод "Предоставить список тестов в заявке для просмотра":
        + 1. Содержимое списка соответствует ожиданиям

    Метод "Предоставить список результатов тестов в заявке для просмотра":
        + 1. Содержимое списка соответствует ожиданиям

    Метод "Внесение результатов теста":
        + 1. У оборудования есть такой тест
        + 2. У оборудования нет такого теста

    Метод "Корректировака результатов тестов":
        + 1. У оборудования есть такой тест
        + 2. У оборудования нет такого теста

    Метод "Завершение технического обслуживания":
        + 1. Сотрудник является инженером бригады
        + 2. Сотрудник не является инженером бригады

    Метод "Подтверждение корректности и закрытие заявки":
        + 1. Сотрудник является бригадиром бригады
        + 2. Сотрудник не является бригадиром бригады

    Метод "Функция которая проверяет состояние оборудования по последднему ТО для него":
        + 1. Оборудование пригодно к использованию
        + 2. Оборудование не пригодно к использованию
"""


# Подготовка данных
# сотрудники
employees = [
    Employee(uid=0, name='Василий'),
    Employee(uid=1, name='Петр'),
    Employee(uid=2, name='Елизавета'),
    Employee(uid=3, name='Артем'),
    Employee(uid=4, name='Евгений'),
    Employee(uid=5, name='Александр'),
]

# список бригад
brigades = [
    Brigade(uid=0, employee_list=employees[0:3], brigadier=employees[0]),
    Brigade(uid=1, employee_list=employees[3:6], brigadier=employees[3]),
]

# Тесты оборудования
equipment_tests = [
    EquipmentTest(name='Проверка диафрагмы', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Чистка temp файлов', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Тестовый запуск', maintenance_type=MaintenanceType.advanced),
    EquipmentTest(name='UNKNOWN', maintenance_type=MaintenanceType.advanced),
]

# даты
commissioning_dates = [
    datetime.now(),
    datetime.now() - timedelta(days=1),
    datetime.now() - timedelta(days=365),
]

# список оборудования
equipments = [
    Equipment(uid=0, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[0]),
    Equipment(uid=1, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[1]),
    Equipment(uid=2, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[2]),
]

# заполнение шлюзов
# шлюз системы учета персонала
pa_system_gateway = PersonnelAccountingSystemGateway()
# шлюз системы учета оборудования
ea_system_gateway = EquipmentAccountingSystemGateway()
# шлюз системы
system_gateway = SystemGatewayInMemory()


# тесты методов реализующих бизнес логику текущей системы
# Метод "Получения заявки" (внутренний метод)
@pytest.mark.system_methods
def test_success_get_maintenance_request():
    """Успешный возврат заявки"""
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_methods._get_and_check_maintenance_request_by_uid(system_gateway, employees[0], 1)
    assert mr.get_uid() == 1


@pytest.mark.system_methods
def test_fail_get_maintenance_request():
    """Заявка уже закрыта"""
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    system_gateway.maintenance_request_service_complete_by_uid(1)
    system_gateway.maintenance_request_service_data_correct_by_uid(1)
    with pytest.raises(ValueError, match="Заявка уже закрыта"):
        system_methods._get_and_check_maintenance_request_by_uid(system_gateway, employees[1], 1)


@pytest.mark.system_methods
def test_mr_service_end_but_employee_not_brigadier():
    """ТО завершено и сотрудник не бригадир бригады"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    system_gateway.maintenance_request_service_complete_by_uid(1)
    with pytest.raises(ValueError, match="Работа с заявкой возможна только для бригадира назначенной бригады"):
        system_methods._get_and_check_maintenance_request_by_uid(system_gateway, employees[1], 1)


@pytest.mark.system_methods
def test_mr_service_not_end_but_employee_not_from_brigade():
    """ТО не завершено и сотрудник не входит в бригаду"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    with pytest.raises(ValueError, match="Работа с заявкой возможна только для сотрудника назначенной бригады"):
        system_methods._get_and_check_maintenance_request_by_uid(system_gateway, employees[3], 1)


# Метод "Выбор самой ненагруженной бригады" (внутренний метод)
@pytest.mark.system_methods
def test_success_get_brigade():
    """Успешное распределение бригад"""
    brigades_list = pa_system_gateway.get_brigades()
    brigades_workload = {0: brigades_list}  # загруженность бригад
    # первый проход
    system_methods._select_brigade(brigades_workload)
    assert brigades_workload.get(0)
    assert len(brigades_workload.get(0)) == 1
    assert brigades_workload.get(1)
    assert len(brigades_workload.get(1)) == 1
    # второй проход
    system_methods._select_brigade(brigades_workload)
    assert not brigades_workload.get(0)
    assert len(brigades_workload.get(1)) == 2
    # третий проход
    system_methods._select_brigade(brigades_workload)
    assert len(brigades_workload.get(1)) == 1
    assert len(brigades_workload.get(2)) == 1


# Метод "Создание заявок"
@pytest.mark.system_methods
def test_success_create_tasks():
    """Успешное создание заявок"""
    system_gateway.clear_maintenance_request_list()
    brigades2 = [
        Brigade(uid=0, employee_list=employees[0:3], brigadier=employees[0]),
        Brigade(uid=1, employee_list=employees[3:6], brigadier=employees[3]),
    ]

    mr_list = system_gateway.get_maintenance_requests()
    mr_list_start_len = len(mr_list)
    with mock.patch.object(EquipmentAccountingSystemGateway, '_request_to_system') as mocked_request_to_system:
        mocked_request_to_system.side_effect = None
        mocked_request_to_system.return_value = {'equipments': equipments, 'equipment_tests': equipment_tests}

        with mock.patch.object(PersonnelAccountingSystemGateway, '_request_to_system') as mocked_request_to_pa_system:
            mocked_request_to_pa_system.side_effect = None
            mocked_request_to_pa_system.return_value = {'brigades': brigades2, 'employees': employees}

            system_methods.create_tasks(system_gateway, ea_system_gateway, pa_system_gateway)
            mr_list = system_gateway.get_maintenance_requests()
            mr_list_new_len = len(mr_list)
            assert mr_list_start_len == 0
            assert mr_list_start_len < mr_list_new_len
            assert mr_list_new_len == 3


# Метод "Предоставить список заявок"
@pytest.mark.system_methods
def test_valid_get_requests_list():
    """Содержимое списка соответствует ожиданиям"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(1)

    with mock.patch.object(PersonnelAccountingSystemGateway, '_request_to_system') as mocked_request_to_pa_system:
        mocked_request_to_pa_system.side_effect = None
        mocked_request_to_pa_system.return_value = {'brigades': brigades, 'employees': employees}

        mr_list_data = system_methods.get_requests_list(system_gateway, pa_system_gateway, 0)
        assert not isinstance(mr_list_data[0], MaintenanceRequest)
        assert mr_list_data[0].get('MR_uid') == mr.get_uid()
        assert mr_list_data[0].get('MR_type') == mr.get_maintenance_type()
        assert mr_list_data[0].get('MR_date_of_completion_of_service') == mr.get_date_of_completion_of_service()
        assert mr_list_data[0].get('MR_closing_date') == mr.get_closing_date()
        assert mr_list_data[0].get('MR_equipment_uid') == equipments[0].get_uid()
        assert mr_list_data[0].get('MR_equipment_commissioning_date') == equipments[0].get_commissioning_date()
        assert mr_list_data[0].get('MR_brigade_uid') == brigades[0].get_uid()


# Метод "Предоставить список тестов в заявке для просмотра"
@pytest.mark.system_methods
def test_valid_get_request_tests_list():
    """Содержимое списка соответствует ожиданиям"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(1)
    tests_list = mr.get_equipment().get_all_tests()

    with mock.patch.object(PersonnelAccountingSystemGateway, '_request_to_system') as mocked_request_to_pa_system:
        mocked_request_to_pa_system.side_effect = None
        mocked_request_to_pa_system.return_value = {'brigades': brigades, 'employees': employees}

        eq_test_list_data = system_methods.get_request_tests_list(system_gateway, pa_system_gateway, 0, 1)
        assert not isinstance(eq_test_list_data[0], EquipmentTest)
        assert eq_test_list_data[0].get('ET_name') == tests_list[0].get_name()
        assert eq_test_list_data[0].get('ET_type') == tests_list[0].get_maintenance_type()


# Метод "Предоставить список результатов тестов в заявке для просмотра"
@pytest.mark.system_methods
def test_valid_get_request_test_results_list():
    """Содержимое списка соответствует ожиданиям"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    system_gateway.add_maintenance_request_test_result_by_uid(1, equipment_tests[0], 'data', employees[0], True)
    mr = system_gateway.get_maintenance_request_by_uid(1)
    test_results_list = mr.get_test_result_list()

    with mock.patch.object(PersonnelAccountingSystemGateway, '_request_to_system') as mocked_request_to_pa_system:
        mocked_request_to_pa_system.side_effect = None
        mocked_request_to_pa_system.return_value = {'brigades': brigades, 'employees': employees}

        mr_tr_list_data = system_methods.get_request_test_results_list(system_gateway, pa_system_gateway, 0, 1)
        test_result = mr_tr_list_data[0]
        assert not isinstance(test_result, RequestTestResult)
        assert test_result.get('MR_TR_name') == test_results_list[0].get_name()
        assert test_result.get('MR_TR_type') == test_results_list[0].get_equipment_test().get_maintenance_type()
        assert test_result.get('MR_TR_data') == test_results_list[0].get_data()
        assert test_result.get('MR_TR_employee_entered_data_uid') == test_results_list[0].get_employee_entered_data().get_uid()
        assert test_result.get('MR_TR_employee_entered_data_name') == test_results_list[0].get_employee_entered_data().get_name()
        assert test_result.get('MR_TR_employee_updated_data_uid') == 'None'
        assert test_result.get('MR_TR_employee_updated_data_name') == 'None'
        assert test_result.get('MR_TR_update_reason') == test_results_list[0].get_update_reason()
        assert test_result.get('MR_TR_state') == test_results_list[0].get_state()


# Метод "Внесение результатов теста"
@pytest.mark.system_methods
def test_success_add_request_test_results():
    """У оборудования есть такой тест"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    system_methods.add_request_test_results(system_gateway, pa_system_gateway, 0, 1, 'Проверка диафрагмы', 'data', True)
    mr_tr = system_gateway.get_maintenance_request_by_uid(1).get_test_result_list()[0]
    assert mr_tr.get_name() == 'Проверка диафрагмы'


@pytest.mark.system_methods
def test_fail_add_request_test_results():
    """У оборудования нет такого теста"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    with pytest.raises(ValueError, match="Такого теста нет у оборудования"):
        system_methods.add_request_test_results(system_gateway, pa_system_gateway, 0, 1, 'UNKNOWN', 'data', True)


# Метод "Корректировака результатов тестов"
@pytest.mark.system_methods
def test_success_update_request_test_results():
    """У оборудования есть такой тест"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    system_methods.add_request_test_results(system_gateway, pa_system_gateway, 0, 1, 'Проверка диафрагмы', 'data', True)
    mr_tr = system_gateway.get_maintenance_request_by_uid(1).get_test_result_list()[0]
    assert mr_tr.get_name() == 'Проверка диафрагмы'
    assert not mr_tr.get_update_reason()
    assert mr_tr.get_employee_entered_data().get_uid() == 0
    assert not mr_tr.get_employee_updated_data()
    system_methods.update_request_test_results(system_gateway, pa_system_gateway, 1, 1,
                                               'Проверка диафрагмы', 'data', 'because I Want')
    mr_tr = system_gateway.get_maintenance_request_by_uid(1).get_test_result_list()[0]
    assert mr_tr.get_name() == 'Проверка диафрагмы'
    assert mr_tr.get_update_reason() == 'because I Want'
    assert mr_tr.get_employee_entered_data().get_uid() == 0
    assert mr_tr.get_employee_updated_data().get_uid() == 1


@pytest.mark.system_methods
def test_fail_update_request_test_results():
    """У оборудования нет такого теста"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    system_methods.add_request_test_results(system_gateway, pa_system_gateway, 0, 1, 'Проверка диафрагмы', 'data', True)
    mr_tr = system_gateway.get_maintenance_request_by_uid(1).get_test_result_list()[0]
    assert mr_tr.get_name() == 'Проверка диафрагмы'
    assert not mr_tr.get_update_reason()
    with pytest.raises(ValueError, match="Такого теста нет у оборудования"):
        system_methods.update_request_test_results(system_gateway, pa_system_gateway, 0, 1,
                                                   'UNKNOWN', 'data', 'because I Want')
    assert not mr_tr.get_update_reason()


# Метод "Завершение технического обслуживания"
@pytest.mark.system_methods
def test_success_service_complete():
    """Сотрудник является инженером бригады"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert not mr.get_date_of_completion_of_service()

    system_methods.service_complete(system_gateway, pa_system_gateway, 1, 1)
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert mr.get_date_of_completion_of_service()


@pytest.mark.system_methods
def test_fail_service_complete():
    """Сотрудник не является инженером бригады"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert not mr.get_date_of_completion_of_service()

    with pytest.raises(ValueError, match="Работа с заявкой возможна только для сотрудника назначенной бригады"):
        system_methods.service_complete(system_gateway, pa_system_gateway, 3, 1)
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert not mr.get_date_of_completion_of_service()


# Метод "Подтверждение корректности и закрытие заявки"
@pytest.mark.system_methods
def test_success_service_data_correct():
    """Сотрудник является бригадиром бригады"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert not mr.get_date_of_completion_of_service()

    system_methods.service_complete(system_gateway, pa_system_gateway, 1, 1)
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert mr.get_date_of_completion_of_service()
    assert not mr.get_closing_date()

    system_methods.service_data_correct(system_gateway, pa_system_gateway, 0, 1)
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert mr.get_date_of_completion_of_service()


@pytest.mark.system_methods
def test_fail_service_data_correct():
    """Сотрудник не является бригадиром бригады"""
    system_gateway.clear_maintenance_request_list()
    system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert not mr.get_date_of_completion_of_service()

    system_methods.service_complete(system_gateway, pa_system_gateway, 1, 1)
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert mr.get_date_of_completion_of_service()
    assert not mr.get_closing_date()

    with pytest.raises(ValueError, match="Работа с заявкой возможна только для бригадира назначенной бригады"):
        system_methods.service_data_correct(system_gateway, pa_system_gateway, 3, 1)
    mr = system_gateway.get_maintenance_request_by_uid(1)
    assert not mr.get_closing_date()


# Метод "Функция которая проверяет состояние оборудования по последднему ТО для него"
@pytest.mark.system_methods
def test_success_check_equipment_state():
    """Оборудование пригодно к использованию"""
    system_gateway.clear_maintenance_request_list()
    ea_system_gateway.clear_equipments_tests_and_equipments_list()

    with mock.patch.object(EquipmentAccountingSystemGateway, '_request_to_system') as mocked_request_to_system:
        mocked_request_to_system.side_effect = None
        mocked_request_to_system.return_value = {'equipments': equipments, 'equipment_tests': equipment_tests}

        system_gateway.add_maintenance_request(equipments[0], brigades[0])
        system_gateway.add_maintenance_request_test_result_by_uid(1, equipment_tests[0], 'data', employees[0], True)
        system_gateway.add_maintenance_request_test_result_by_uid(1, equipment_tests[1], 'data', employees[1], True)
        system_gateway.add_maintenance_request_test_result_by_uid(1, equipment_tests[2], 'data', employees[2], True)
        system_gateway.maintenance_request_service_complete_by_uid(1)
        system_gateway.maintenance_request_service_data_correct_by_uid(1)

        system_methods.check_equipment_state(system_gateway, ea_system_gateway, 0)


@pytest.mark.system_methods
def test_fail_check_equipment_state():
    """Оборудование не пригодно к использованию"""
    system_gateway.clear_maintenance_request_list()
    ea_system_gateway.clear_equipments_tests_and_equipments_list()

    with mock.patch.object(EquipmentAccountingSystemGateway, '_request_to_system') as mocked_request_to_system:
        mocked_request_to_system.side_effect = None
        mocked_request_to_system.return_value = {'equipments': equipments, 'equipment_tests': equipment_tests}

        system_gateway.add_maintenance_request(equipments[0], brigades[0])
        system_gateway.add_maintenance_request_test_result_by_uid(1, equipment_tests[0], 'data', employees[0], True)
        system_gateway.add_maintenance_request_test_result_by_uid(1, equipment_tests[1], 'invalid', employees[1], False)
        system_gateway.add_maintenance_request_test_result_by_uid(1, equipment_tests[2], 'data', employees[2], True)
        system_gateway.maintenance_request_service_complete_by_uid(1)
        system_gateway.maintenance_request_service_data_correct_by_uid(1)

        with pytest.raises(ValueError, match="Оборудование непригодно к эксплуатации"):
            system_methods.check_equipment_state(system_gateway, ea_system_gateway, 0)
