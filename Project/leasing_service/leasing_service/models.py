# coding=utf-8
from django.db import models


class RequestTestResult:  # (models.Model)
    """Информация о результатах теста"""
    equipment_test = ''
    name = models.CharField(max_length=255, blank=True, null=True)
    data = ''
    state = ''
    employee_entered_data_uid = ''
    employee_updated_data_uid = None
    update_reason = None


class MaintenanceRequest:  # (models.Model)
    """Информация о заявке на ТО"""
    uid = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    maintenance_type = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    maintenance_request_state = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)

    equipment_uid = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    brigade_uid = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    test_result_list = ''  # FK

    opening_date = models.DateTimeField(max_digits=12, decimal_places=2, null=True, blank=True)
    date_of_completion_of_service = models.DateTimeField(max_digits=12, decimal_places=2, null=True, blank=True)
    closing_date = models.DateTimeField(max_digits=12, decimal_places=2, null=True, blank=True)
