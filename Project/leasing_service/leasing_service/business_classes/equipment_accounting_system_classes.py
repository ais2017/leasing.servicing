# coding=utf-8
"""
Данный модуль содержит классы для работы с данными полученными из системы учета оборудования
"""

from datetime import datetime
from enum import IntEnum
from typing import List


class MaintenanceType(IntEnum):
    """
    Перечисление с допустимыми типами ТО (обычное / расширенное)
    """
    simple = 1
    advanced = 2


class EquipmentTest(object):
    """
    Информация о тесте оборудования
    """

    def __new__(cls, name: str, maintenance_type: MaintenanceType):
        """Инициализация класса тест оборудования с проверкой данных"""
        if not name:  # пустой name недопустим
            return None
        if maintenance_type not in MaintenanceType:  # невалидный maintenance_type недопустим
            return None
        # инициализируем
        instance = super(EquipmentTest, cls).__new__(cls)
        return instance

    def __init__(self, name: str, maintenance_type: MaintenanceType):
        """Новый тест оборудования - название, тип ТО"""
        self._name = name
        self._maintenance_type = maintenance_type

    def get_name(self) -> str:
        """Отдать название"""
        return self._name

    def get_maintenance_type(self) -> MaintenanceType:
        """Отдать тип ТО"""
        return self._maintenance_type


class Equipment(object):
    """
    Информация об оборудовании
    """

    def __new__(cls, uid: int, equipment_test_list: List, commissioning_date: datetime):
        """Инициализация класса оборудование с проверкой данных"""
        if not isinstance(uid, int):
            return None  # не числовой uid недопустим
        if not isinstance(equipment_test_list, list):
            return None  # equipment_test_list должен быть списком
        if len(equipment_test_list) == 0:
            return None  # equipment_test_list должен быть не пустым
        for equipment_test in equipment_test_list:
            if not isinstance(equipment_test, EquipmentTest):
                return None  # equipment_test_list может содержать только EquipmentTest
        if not isinstance(commissioning_date, datetime):
            return None  # commissioning_date должен быть датой
        # инициализируем
        instance = super(Equipment, cls).__new__(cls)
        return instance

    def __init__(self, uid: int, equipment_test_list: List, commissioning_date: datetime):
        """Новое оборудование - список тестов оборудования, дата ввода в эксплуатацию"""
        self._uid = uid
        self._equipment_test_list = equipment_test_list
        self._commissioning_date = commissioning_date

    def get_uid(self) -> int:
        """Отдать уникальный идентификатор"""
        return self._uid

    def get_tests_by_maintenance_type(self, maintenance_type: MaintenanceType) -> List:
        """Отдать список тестов по типу ТО"""
        tests_list = list()
        for test in self._equipment_test_list:
            if test.get_maintenance_type() == maintenance_type:
                tests_list.append(test)
        return tests_list

    def get_all_tests(self) -> List:
        """Отдать список всех тестов"""
        return self._equipment_test_list

    def get_test_by_name(self, name: str):
        """Отдать тест по названию"""
        for test in self._equipment_test_list:
            if test.get_name() == name:
                return test
        return None  # тест не найден

    def get_commissioning_date(self) -> datetime:
        """Отдать дату ввода в эксплуатацию"""
        return self._commissioning_date
