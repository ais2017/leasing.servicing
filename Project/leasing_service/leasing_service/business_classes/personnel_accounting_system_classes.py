# coding=utf-8
"""
Данный модуль содержит классы для работы с данными полученными из системы учета персонала
"""

from typing import List


class Employee(object):
    """
    Информация о сотруднике
    """

    def __new__(cls, uid: int, name: str):
        """Инициализация класса сотрудник с проверкой данных"""
        if not isinstance(uid, int):  # не числовой uid недопустим
            return None
        if not name:  # пустой name недопустим
            return None
        # инициализируем
        instance = super(Employee, cls).__new__(cls)
        return instance

    def __init__(self, uid: int, name: str):
        """Новый сотрудник - уникальный идентификатор, имя"""
        self._uid = uid
        self._name = name

    def get_uid(self) -> int:
        """Отдать уникальный идентификатор"""
        return self._uid

    def get_name(self) -> str:
        """Отдать имя"""
        return self._name


class Brigade(object):
    """
    Информация о бригаде
    """

    def __new__(cls, uid: int, employee_list: List, brigadier: Employee):
        """Инициализация класса бригадир с проверкой данных"""
        if not isinstance(uid, int):  # uid должен быть Integer
            return None
        if not isinstance(employee_list, list):  # employee_list может быть пустым, но обязательно списком
            return None
        for employee in employee_list:  # employee_list может содержать только Employee
            if not isinstance(employee, Employee):
                return None
        if not isinstance(brigadier, Employee):  # brigadier должен быть Employee
            return None
        # инициализируем
        instance = super(Brigade, cls).__new__(cls)
        return instance

    def __init__(self, uid: int, employee_list: List, brigadier: Employee):
        """Новая бригада - уникальный идентификатор, список сотрудников, бригадир"""
        self._uid = uid
        self._employee_list = employee_list
        self._brigadier = brigadier

    def get_uid(self) -> int:
        """Отдать уникальный идентификатор"""
        return self._uid

    def get_brigadier(self) -> Employee:
        """Отдать бригадира"""
        return self._brigadier

    def get_employee_list(self) -> List:
        """Отдать список сотрудников"""
        return self._employee_list

    def find_employee_by_uid(self, employee_uid: int):
        """Отдать сотрудника по идентификатору (или None если такого нет)"""
        for empl in self._employee_list:
            if empl.get_uid() == employee_uid:
                return empl
        return None
