# coding=utf-8
import pytest

from leasing_service.business_classes.personnel_accounting_system_classes import Employee, Brigade

"""
Чек-лист классов для работы с данными полученными из системы учета персонала:

    Чек-лист класса Employee:
    + 1. Проверка получаемых из getter результатов
    + 2. Проверка инициализации

    Чек-лист класса Brigade:
    + 1. Проверка на корректность данных о бригадире и уникальном идентификаторе
    + 2. Проверка на корректность данных о сотрудниках в списке сотрудников бригады
    + 3. Проверка поиска сотрудника по uid
    + 4. Проверка инициализации
"""


# сотрудники
employees = [
    Employee(uid=0, name='Василий'),
    Employee(uid=1, name='Петр'),
    Employee(uid=2, name='Елизавета'),
    Employee(uid=3, name='Артем'),
    Employee(uid=4, name='Евгений'),
    Employee(uid=5, name='Александр'),
    Employee(uid=6, name='Максим'),
    Employee(uid=7, name='Юлия'),
    Employee(uid=8, name='Мария'),
]

# списки сотрудников (для бригад)
employees_lists = [
    employees[0:3],
    employees[3:6],
    employees[6:9],
]

# список бригад
brigades = [
    Brigade(uid=0, employee_list=employees_lists[0], brigadier=employees_lists[0][0]),
    Brigade(uid=1, employee_list=employees_lists[1], brigadier=employees_lists[1][0]),
    Brigade(uid=2, employee_list=employees_lists[2], brigadier=employees_lists[2][0]),
]


# тесты Employee
@pytest.mark.personnel_accounting_system_classes
def test_employee_getter():
    """Проверка получаемых из getter результатов"""
    employee = employees[0]
    assert employee.get_uid() == 0
    assert employee.get_name() == 'Василий'


@pytest.mark.personnel_accounting_system_classes
def test_employee_init():
    """Проверка инициализации"""
    employee = Employee(uid=None, name='Василий')
    assert employee is None  # uid должен быть Integer
    employee = Employee(uid='', name='Василий')
    assert employee is None  # uid должен быть Integer
    employee = Employee(uid=0, name=None)
    assert employee is None  # name обязателен
    employee = Employee(uid=0, name='')
    assert employee is None  # name обязателен
    employee = Employee(uid=0, name='Василий')
    assert employee is not None


# тесты Brigade
@pytest.mark.personnel_accounting_system_classes
def test_brigade_correct_brigadier():
    """Проверка на корректность данных о бригадире и уникальном идентификаторе"""
    brigade = brigades[0]
    brigadier = employees_lists[0][0]
    assert brigade.get_uid() == 0
    assert brigade.get_brigadier() == brigadier


@pytest.mark.personnel_accounting_system_classes
def test_brigade_correct_employees_list():
    """Проверка на корректность данных о сотрудниках в списке сотрудников бригады"""
    brigade = brigades[1]
    employees_list = employees_lists[1]
    assert brigade.get_employee_list() == employees_list


@pytest.mark.personnel_accounting_system_classes
def test_brigade_find_employee_by_uid():
    """Проверка поиска сотрудника по uid"""
    brigade = brigades[2]
    employee = employees_lists[2][2]
    assert brigade.find_employee_by_uid(8) == employee
    assert brigade.find_employee_by_uid(0) is None


@pytest.mark.personnel_accounting_system_classes
def test_brigade_init():
    """Проверка инициализации"""
    brigade = Brigade(uid=None, employee_list=employees_lists[0], brigadier=employees_lists[0][0])
    assert brigade is None  # uid должен быть Integer
    brigade = Brigade(uid='', employee_list=employees_lists[0], brigadier=employees_lists[0][0])
    assert brigade is None  # uid должен быть Integer
    brigade = Brigade(uid=0, employee_list=employees_lists[0], brigadier=None)
    assert brigade is None  # brigadier должен быть Employee
    brigade = Brigade(uid=0, employee_list=employees_lists[0], brigadier=1)
    assert brigade is None  # brigadier должен быть Employee
    brigade = Brigade(uid=0, employee_list=None, brigadier=employees_lists[0][0])
    assert brigade is None  # employee_list может быть пустым но обязательно списком
    brigade = Brigade(uid=0, employee_list='', brigadier=employees_lists[0][0])
    assert brigade is None  # employee_list может быть пустым но обязательно списком
    brigade = Brigade(uid=0, employee_list=[None, ], brigadier=employees_lists[0][0])
    assert brigade is None  # employee_list может содержать только Employee
    brigade = Brigade(uid=0, employee_list=[employees[0], None, ], brigadier=employees_lists[0][0])
    assert brigade is None  # employee_list может содержать только Employee
    brigade = Brigade(uid=0, employee_list=[], brigadier=employees_lists[0][0])
    assert brigade is not None  # employee_list может быть пустым но обязательно списком
    brigade = Brigade(uid=0, employee_list=[employees[0], ], brigadier=employees_lists[0][0])
    assert brigade is not None  # employee_list может содержать только Employee
    brigade = Brigade(uid=0, employee_list=employees_lists[0], brigadier=employees_lists[0][0])
    assert brigade is not None
