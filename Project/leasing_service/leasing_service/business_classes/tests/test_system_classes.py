# coding=utf-8
from datetime import datetime, timedelta

import pytest

from leasing_service.business_classes.equipment_accounting_system_classes \
    import MaintenanceType, EquipmentTest, Equipment
from leasing_service.business_classes.personnel_accounting_system_classes import Employee, Brigade
from leasing_service.business_classes.system_classes \
    import MaintenanceRequestState, RequestTestResult, MaintenanceRequest

"""
Чек-лист классов для работы с данными текущей системы (обслуживание оборудования):

    Чек-лист для enum MaintenanceRequestState:
    + 1. Проверка получаемых из enum значений

    Чек-лист класса RequestTestResult:
    + 1. Проверка получаемых из getter результатов сразу после создания
    + 2. Проверка получаемых из getter результатов после обновления
    + 3. Проверка инициализации

    Чек-лист класса MaintenanceRequest:
    + 1. Проверка получаемых из getter результатов сразу после создания
    + 2. Проверка добавления, обновления и поиска теста по имени
    + 3. Проверка изменения дат и статуса при завершении обслуживания и подтверждении корректности
    + 4. Проверка невозможности подтверждении корректности до завершения обслуживания
    + 5. Попытка добавления двух тестов с одинаковым именем и теста не входящего в список тестов оборудования
    + 6. Попытка обновления несуществующего теста
    + 7. Попытка добавления/обновления теста после закрытия заявки
    + 8. Попытка повторного завершения обслуживания и повторного подтверждения корректности
    + 9. Проверка инициализации
    + 10. Проверка поиска результата теста по тесту оборудования
"""

# сотрудники
employees = [
    Employee(uid=0, name='Василий'),
    Employee(uid=1, name='Петр'),
    Employee(uid=2, name='Елизавета'),
    Employee(uid=3, name='Артем'),
    Employee(uid=4, name='Евгений'),
    Employee(uid=5, name='Александр'),
]

# список бригад
brigades = [
    Brigade(uid=0, employee_list=employees[0:3], brigadier=employees[0]),
    Brigade(uid=1, employee_list=employees[3:6], brigadier=employees[3]),
]

# Тесты оборудования
equipment_tests = [
    EquipmentTest(name='Проверка диафрагмы', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Чистка temp файлов', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Тестовый запуск', maintenance_type=MaintenanceType.advanced),
    EquipmentTest(name='UNKNOWN', maintenance_type=MaintenanceType.advanced),
]

# даты
commissioning_dates = [
    datetime.now(),
    datetime.now() - timedelta(days=1),
    datetime.now() - timedelta(days=365),
]

# список оборудования
equipments = [
    Equipment(uid=0, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[0]),
    Equipment(uid=1, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[1]),
    Equipment(uid=2, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[2]),
]


# тесты enum MaintenanceType
@pytest.mark.system_classes
def test_maintenance_request_states():
    """Проверка получаемых из enum значений"""
    assert MaintenanceRequestState.is_open == 1
    assert MaintenanceRequestState.service_completed == 2
    assert MaintenanceRequestState.is_closed == 3


# тесты RequestTestResult
@pytest.mark.system_classes
def test_request_test_result_getters_after_create():
    """Проверка получаемых из getter результатов сразу после создания"""
    request_test_result = RequestTestResult(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    assert request_test_result.get_equipment_test() == equipment_tests[0]
    assert request_test_result.get_name() == 'Проверка диафрагмы'
    assert request_test_result.get_data() == 'Результат 1'
    assert request_test_result.get_employee_entered_data() == employees[1]
    assert request_test_result.get_employee_updated_data() is None
    assert request_test_result.get_update_reason() is None


@pytest.mark.system_classes
def test_request_test_result_getters_after_update():
    """Проверка получаемых из getter результатов после обновления"""
    request_test_result = RequestTestResult(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    assert request_test_result.get_data() == 'Результат 1'

    # пустая data недопустима
    result = request_test_result.update(
        data=None,
        employee_updated_data=employees[0],
        update_reason='Некорректное значение люфта')
    assert result is None
    assert request_test_result.get_data() == 'Результат 1'
    assert request_test_result.get_employee_updated_data() is None
    assert request_test_result.get_update_reason() is None

    # employee_updated_data должен быть Employee
    result = request_test_result.update(
        data='Результат 0.10',
        employee_updated_data=None,
        update_reason='Некорректное значение люфта')
    assert result is None
    assert request_test_result.get_data() == 'Результат 1'
    assert request_test_result.get_employee_updated_data() is None
    assert request_test_result.get_update_reason() is None

    # пустой update_reason недопустим
    result = request_test_result.update(
        data='Результат 0.10',
        employee_updated_data=employees[0],
        update_reason=None)
    assert result is None
    assert request_test_result.get_data() == 'Результат 1'
    assert request_test_result.get_employee_updated_data() is None
    assert request_test_result.get_update_reason() is None

    # успешное обновление
    result = request_test_result.update(
        data='Результат 0.10',
        employee_updated_data=employees[0],
        update_reason='Некорректное значение люфта')
    assert isinstance(result, RequestTestResult)
    assert request_test_result.get_equipment_test() == equipment_tests[0]
    assert request_test_result.get_name() == 'Проверка диафрагмы'
    assert request_test_result.get_data() == 'Результат 0.10'
    assert request_test_result.get_employee_entered_data() == employees[1]
    assert request_test_result.get_employee_updated_data() == employees[0]
    assert request_test_result.get_update_reason() == 'Некорректное значение люфта'


@pytest.mark.system_classes
def test_request_test_result_init():
    """Проверка инициализации"""
    request_test_result = RequestTestResult(
        equipment_test=None,
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    assert request_test_result is None  # equipment_test должен быть EquipmentTest

    request_test_result = RequestTestResult(
        equipment_test=1,
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    assert request_test_result is None  # equipment_test должен быть EquipmentTest

    request_test_result = RequestTestResult(
        equipment_test=equipment_tests[0],
        data=None,
        employee_entered_data=employees[1],
        state=True)
    assert request_test_result is None  # пустая data недопустима

    request_test_result = RequestTestResult(
        equipment_test=equipment_tests[0],
        data='',
        employee_entered_data=employees[1],
        state=True)
    assert request_test_result is None  # пустая data недопустима

    request_test_result = RequestTestResult(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=None,
        state=True)
    assert request_test_result is None  # employee_entered_data должен быть Employee

    request_test_result = RequestTestResult(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=123,
        state=True)
    assert request_test_result is None  # employee_entered_data должен быть Employee

    request_test_result = RequestTestResult(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    assert request_test_result is not None


# тесты MaintenanceRequest
@pytest.mark.system_classes
def test_maintenance_request_getters_after_create():
    """Проверка получаемых из getter результатов сразу после создания"""
    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)
    assert maintenance_request.get_uid() == 0
    assert maintenance_request.get_equipment() == equipments[0]
    assert maintenance_request.get_brigade() == brigades[0]
    assert maintenance_request.get_maintenance_type() == MaintenanceType.simple
    assert maintenance_request.get_opening_date() <= datetime.now()
    assert maintenance_request.get_maintenance_request_state() == MaintenanceRequestState.is_open
    assert len(maintenance_request.get_test_result_list()) == 0
    assert maintenance_request.get_date_of_completion_of_service() is None
    assert maintenance_request.get_closing_date() is None


@pytest.mark.personnel_accounting_system_classes
def test_maintenance_request_add_update_and_find_test_by_name():
    """Проверка добавления, обновления и поиска теста по имени"""
    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)

    # неудачное добавление теста (кривые данные)
    result = maintenance_request.add_test_result(
        equipment_test=None,
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    assert not result

    result = maintenance_request.add_test_result(
        equipment_test=equipment_tests[0],
        data=None,
        employee_entered_data=employees[1],
        state=True)
    assert not result

    result = maintenance_request.add_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=None,
        state=True)
    assert not result

    # удачное добавление теста
    result = maintenance_request.add_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    assert isinstance(result, RequestTestResult)
    request_test_result = maintenance_request.get_test_result_by_name(name='Проверка диафрагмы')
    assert request_test_result.get_name() == 'Проверка диафрагмы'
    assert request_test_result.get_data() == 'Результат 1'
    assert request_test_result.get_employee_entered_data() == employees[1]

    # неудачное обновление теста (кривые данные)
    result = maintenance_request.update_test_result(
        equipment_test=None,
        data='Результат 0.001',
        employee_updated_data=employees[0],
        update_reason='Некорректное значение люфта')
    assert result is None

    result = maintenance_request.update_test_result(
        equipment_test=equipment_tests[0],
        data=None,
        employee_updated_data=employees[0],
        update_reason='Некорректное значение люфта')
    assert result is None

    result = maintenance_request.update_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 0.001',
        employee_updated_data=None,
        update_reason='Некорректное значение люфта')
    assert result is None

    result = maintenance_request.update_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 0.001',
        employee_updated_data=employees[0],
        update_reason=None)
    assert result is None

    # удачное обновление теста
    result = maintenance_request.update_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 0.001',
        employee_updated_data=employees[0],
        update_reason='Некорректное значение люфта')
    assert isinstance(result, RequestTestResult)

    # поиск существующего теста по имени
    request_test_result = maintenance_request.get_test_result_by_name(name='Проверка диафрагмы')
    assert request_test_result.get_name() == 'Проверка диафрагмы'
    assert request_test_result.get_data() == 'Результат 0.001'
    assert request_test_result.get_employee_entered_data() == employees[1]
    assert request_test_result.get_employee_updated_data() == employees[0]
    assert request_test_result.get_update_reason() == 'Некорректное значение люфта'

    # поиск не существующего теста по имени
    request_test_result = maintenance_request.get_test_result_by_name(name='UNKNOWN NAME')
    assert request_test_result is None


@pytest.mark.system_classes
def test_maintenance_request_date_and_statuses():
    """Проверка изменения дат и статуса при завершении обслуживания и подтверждении корректности"""
    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)
    assert maintenance_request.get_maintenance_request_state() == MaintenanceRequestState.is_open
    assert maintenance_request.get_date_of_completion_of_service() is None
    assert maintenance_request.get_closing_date() is None

    # завершение обслуживания
    result = maintenance_request.service_complete()
    assert maintenance_request.get_maintenance_request_state() == MaintenanceRequestState.service_completed
    assert maintenance_request.get_date_of_completion_of_service() is not None
    assert maintenance_request.get_closing_date() is None
    assert result

    # подтверждение корректности
    result = maintenance_request.service_data_correct()
    assert maintenance_request.get_maintenance_request_state() == MaintenanceRequestState.is_closed
    assert maintenance_request.get_date_of_completion_of_service() is not None
    assert maintenance_request.get_closing_date() is not None
    assert result


@pytest.mark.system_classes
def test_maintenance_request_service_data_correct_before_service_complete():
    """Проверка невозможности подтверждении корректности до завершения обслуживания"""
    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)

    # подтверждение корректности до завершения обслуживания не даёт эффекта
    result = maintenance_request.service_data_correct()
    assert maintenance_request.get_maintenance_request_state() == MaintenanceRequestState.is_open
    assert maintenance_request.get_date_of_completion_of_service() is None
    assert maintenance_request.get_closing_date() is None
    assert not result


@pytest.mark.system_classes
def test_maintenance_request_two_test_with_one_name():
    """Попытка добавления двух тестов с одинаковым именем и теста не входящего в список тестов оборудования"""
    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)
    result = maintenance_request.add_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    assert len(maintenance_request.get_test_result_list()) == 1
    assert isinstance(result, RequestTestResult)

    # повторное добавление того же теста не добавляет тест
    result = maintenance_request.add_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 2',
        employee_entered_data=employees[2],
        state=True)
    assert len(maintenance_request.get_test_result_list()) == 1
    assert not result

    # Тест не входит в список тестов оборудования указанного в заявке
    result = maintenance_request.add_test_result(
        equipment_test=equipment_tests[3],
        data='Результат 3',
        employee_entered_data=employees[2],
        state=True)
    assert len(maintenance_request.get_test_result_list()) == 1
    assert not result


@pytest.mark.system_classes
def test_maintenance_request_try_to_update_not_existing_test():
    """Попытка обновления несуществующего теста"""
    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)

    # попытка обновить несуществующий тест
    result = maintenance_request.update_test_result(
        equipment_test=equipment_tests[3],
        data='Результат 0.001',
        employee_updated_data=employees[0],
        update_reason='Некорректное значение люфта')
    assert len(maintenance_request.get_test_result_list()) == 0
    assert result is None


@pytest.mark.system_classes
def test_maintenance_request_try_add_update_after_close_request():
    """Попытка добавления/обновления теста после закрытия заявки"""
    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)

    # закрытие заявки
    maintenance_request.service_complete()
    maintenance_request.service_data_correct()

    # попытка добавить тест вернёт false
    result = maintenance_request.add_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    assert len(maintenance_request.get_test_result_list()) == 0
    assert not result

    # попытка обновить тест вернёт false
    result = maintenance_request.update_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 0.001',
        employee_updated_data=employees[0],
        update_reason='Некорректное значение люфта')
    assert not result


@pytest.mark.system_classes
def test_maintenance_request_repeat_service_complete_and_data_correct():
    """Попытка повторного завершения обслуживания и повторного подтверждения корректности"""
    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)

    # Двойное завершение обслуживания (только первое успех)
    result = maintenance_request.service_complete()
    date_of_completion = maintenance_request.get_date_of_completion_of_service()
    assert maintenance_request.get_date_of_completion_of_service() is not None
    assert result
    result = maintenance_request.service_complete()
    assert maintenance_request.get_date_of_completion_of_service() == date_of_completion
    assert not result

    # Двойное подтверждение корректности (только первое успех)
    result = maintenance_request.service_data_correct()
    date_of_close = maintenance_request.get_closing_date()
    assert maintenance_request.get_closing_date() is not None
    assert result
    result = maintenance_request.service_data_correct()
    assert maintenance_request.get_closing_date() == date_of_close
    assert not result


@pytest.mark.system_classes
def test_maintenance_request_init():
    """Проверка инициализации"""
    maintenance_request = MaintenanceRequest(
        uid=None,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)
    assert maintenance_request is None  # не числовой uid недопустим

    maintenance_request = MaintenanceRequest(
        uid='',
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)
    assert maintenance_request is None  # не числовой uid недопустим

    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=None,
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)
    assert maintenance_request is None  # equipment должен быть Equipment

    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=None,
        maintenance_type=MaintenanceType.simple)
    assert maintenance_request is None  # brigade должен быть Brigade

    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=None)
    assert maintenance_request is None  # невалидный maintenance_type недопустим

    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)
    assert maintenance_request is not None


@pytest.mark.system_classes
def test_maintenance_request_get_test_result_by_equipment_test():
    """Проверка поиска результата теста по тесту оборудования"""
    maintenance_request = MaintenanceRequest(
        uid=0,
        equipment=equipments[0],
        brigade=brigades[0],
        maintenance_type=MaintenanceType.simple)

    # добавление результата теста который затем будем искать
    result = maintenance_request.add_test_result(
        equipment_test=equipment_tests[0],
        data='Результат 1',
        employee_entered_data=employees[1],
        state=True)
    result_test = maintenance_request.get_test_result_by_name(name='Проверка диафрагмы')

    assert result
    assert maintenance_request.get_test_result_by_equipment_test(equipment_test=equipment_tests[0]) == result_test
    assert maintenance_request.get_test_result_by_equipment_test(equipment_test=equipment_tests[1]) is None
