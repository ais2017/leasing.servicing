# coding=utf-8
from datetime import datetime, timedelta

import pytest

from leasing_service.business_classes.equipment_accounting_system_classes \
    import MaintenanceType, EquipmentTest, Equipment

"""
Чек-лист классов для работы с данными полученными из системы учета оборудования:

    Чек-лист для enum MaintenanceType:
    + 1. Проверка получаемых из enum значений

    Чек-лист класса EquipmentTest:
    + 1. Проверка получаемых из getter результатов
    + 2. Проверка инициализации

    Чек-лист класса Equipment:
    + 1. Проверка на корректность даты ввода в эксплуатацию и уникального идентификатора
    + 2. Проверка на корректность полного списка тестов
    + 3. Проверка на корректность списка тестов по типу ТО
    + 4. Проверка инициализации
    + 5. Проверка получения теста оборудования по его названию
"""


# Тесты оборудования
equipment_tests = [
    EquipmentTest(name='Проверка диафрагмы', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Чистка temp файлов', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Тестовый запуск', maintenance_type=MaintenanceType.advanced),
    EquipmentTest(name='3', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='4', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='5', maintenance_type=MaintenanceType.advanced),
    EquipmentTest(name='6', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='7', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='8', maintenance_type=MaintenanceType.advanced),
]

# списки тестов оборудования (для оборудования)
equipment_tests_lists = [
    equipment_tests[0:3],
    equipment_tests[3:6],
    equipment_tests[6:9],
]

# даты
commissioning_dates = [
    datetime.now(),
    datetime.now() - timedelta(days=1),
    datetime.now() - timedelta(days=365),
]

# список оборудования
equipments = [
    Equipment(uid=0, equipment_test_list=equipment_tests_lists[0], commissioning_date=commissioning_dates[0]),
    Equipment(uid=1, equipment_test_list=equipment_tests_lists[1], commissioning_date=commissioning_dates[1]),
    Equipment(uid=2, equipment_test_list=equipment_tests_lists[2], commissioning_date=commissioning_dates[2]),
]


# тесты enum MaintenanceType
@pytest.mark.equipment_accounting_system_classes
def test_maintenance_types():
    """Проверка получаемых из enum значений"""
    assert MaintenanceType.simple == 1
    assert MaintenanceType.advanced == 2


# тесты EquipmentTest
@pytest.mark.equipment_accounting_system_classes
def test_equipment_test_getter():
    """Проверка получаемых из getter результатов"""
    equipment_test = equipment_tests[0]
    assert equipment_test.get_maintenance_type() == MaintenanceType.simple
    assert equipment_test.get_maintenance_type() == 1
    assert equipment_test.get_name() == 'Проверка диафрагмы'
    equipment_test = equipment_tests[2]
    assert equipment_test.get_maintenance_type() == MaintenanceType.advanced
    assert equipment_test.get_maintenance_type() == 2
    assert equipment_test.get_name() == 'Тестовый запуск'


@pytest.mark.equipment_accounting_system_classes
def test_equipment_test_init():
    """Проверка инициализации"""
    equipment_test = EquipmentTest(name=None, maintenance_type=MaintenanceType.simple)
    assert equipment_test is None  # name обязателен
    equipment_test = EquipmentTest(name='', maintenance_type=MaintenanceType.simple)
    assert equipment_test is None  # name обязателен
    equipment_test = EquipmentTest(name='Проверка диафрагмы', maintenance_type=None)
    assert equipment_test is None  # невалидный maintenance_type недопустим
    equipment_test = EquipmentTest(name='Проверка диафрагмы', maintenance_type=0)
    assert equipment_test is None  # невалидный maintenance_type недопустим
    equipment_test = EquipmentTest(name='Проверка диафрагмы', maintenance_type=MaintenanceType.simple)
    assert equipment_test is not None


# тесты Equipment
@pytest.mark.equipment_accounting_system_classes
def test_equipment_get_commissioning_date():
    """Проверка на корректность даты ввода в эксплуатацию и уникального идентификатора"""
    assert equipments[0].get_commissioning_date() == commissioning_dates[0]
    assert equipments[1].get_commissioning_date() == commissioning_dates[1]
    assert equipments[2].get_commissioning_date() == commissioning_dates[2]
    assert equipments[0].get_uid() == 0
    assert equipments[1].get_uid() == 1
    assert equipments[2].get_uid() == 2


@pytest.mark.equipment_accounting_system_classes
def test_equipment_get_all_tests():
    """Проверка на корректность полного списка тестов"""
    equipment = equipments[0]
    assert equipment.get_all_tests() == equipment_tests_lists[0]


@pytest.mark.equipment_accounting_system_classes
def test_equipment_get_tests_by_maintenance_type():
    """Проверка на корректность списка тестов по типу ТО"""
    equipment = equipments[1]
    assert equipment.get_tests_by_maintenance_type(MaintenanceType.simple) == [equipment_tests[3], equipment_tests[4], ]
    assert equipment.get_tests_by_maintenance_type(MaintenanceType.advanced) == [equipment_tests[5], ]


@pytest.mark.equipment_accounting_system_classes
def test_equipment_init():
    """Проверка инициализации"""
    equipment = Equipment(uid=None, equipment_test_list=equipment_tests_lists[0], commissioning_date=commissioning_dates[0])
    assert equipment is None  # не числовой uid недопустим
    equipment = Equipment(uid='', equipment_test_list=equipment_tests_lists[0], commissioning_date=commissioning_dates[0])
    assert equipment is None  # не числовой uid недопустим
    equipment = Equipment(uid=0, equipment_test_list=equipment_tests_lists[0], commissioning_date=None)
    assert equipment is None  # commissioning_date должен быть датой
    equipment = Equipment(uid=0, equipment_test_list=equipment_tests_lists[0], commissioning_date='2017-10-10')
    assert equipment is None  # commissioning_date должен быть датой
    equipment = Equipment(uid=0, equipment_test_list=None, commissioning_date=commissioning_dates[0])
    assert equipment is None  # equipment_test_list должен быть списком
    equipment = Equipment(uid=0, equipment_test_list=list(), commissioning_date=commissioning_dates[0])
    assert equipment is None   # equipment_test_list не должен быть пустым
    equipment = Equipment(uid=0, equipment_test_list=[None, ], commissioning_date=commissioning_dates[0])
    assert equipment is None   # equipment_test_list может содержать только EquipmentTest
    equipment = Equipment(uid=0, equipment_test_list=[equipment_tests[0], None, ], commissioning_date=commissioning_dates[0])
    assert equipment is None   # equipment_test_list может содержать только EquipmentTest
    equipment = Equipment(uid=0, equipment_test_list=[equipment_tests[0], ], commissioning_date=commissioning_dates[0])
    assert equipment is not None   # equipment_test_list должен содержать хоть один EquipmentTest
    equipment = Equipment(uid=0, equipment_test_list=equipment_tests_lists[0], commissioning_date=commissioning_dates[0])
    assert equipment is not None


@pytest.mark.equipment_accounting_system_classes
def test_equipment_get_test_by_name():
    """Проверка получения теста оборудования по его названию"""
    equipment = equipments[2]
    assert equipment.get_test_by_name('8') == equipment_tests[8]
    assert equipment.get_test_by_name('Проверка диафрагмы') is None
