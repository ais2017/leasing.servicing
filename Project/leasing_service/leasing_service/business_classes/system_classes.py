# coding=utf-8
"""
Данный модуль содержит классы для работы с данными текущей системы (обслуживание оборудования)
"""

from datetime import datetime
from enum import IntEnum
from typing import List

from leasing_service.business_classes.equipment_accounting_system_classes \
    import MaintenanceType, EquipmentTest, Equipment
from leasing_service.business_classes.personnel_accounting_system_classes import Employee, Brigade


class MaintenanceRequestState(IntEnum):
    """
    Перечисление с допустимыми состояниями заявки на ТО
    (Открыта / Обслуживание завершено / Закрыта)
    """
    is_open = 1
    service_completed = 2
    is_closed = 3


class RequestTestResult(object):
    """
    Информация о результатах теста
    """

    def __new__(cls, equipment_test: EquipmentTest, data: str, employee_entered_data: Employee, state: bool):
        """Инициализация класса результат теста с проверкой данных"""
        if not isinstance(equipment_test, EquipmentTest):  # equipment_test должен быть EquipmentTest
            return None
        if not data:  # пустая data недопустима
            return None
        if not isinstance(employee_entered_data, Employee):  # employee_entered_data должен быть Employee
            return None
        if not isinstance(state, bool):  # state должен быть bool
            return None
        # инициализируем
        instance = super(RequestTestResult, cls).__new__(cls)
        return instance

    def __init__(self, equipment_test: EquipmentTest, data: str, employee_entered_data: Employee, state: bool):
        """Новый результат теста - имя теста, данные, сотрудник который ввел данные, статус теста (успешен / нет)"""
        self._equipment_test = equipment_test
        self._name = equipment_test.get_name()
        self._data = data
        self._state = state
        self._employee_entered_data = employee_entered_data
        self._employee_updated_data = None
        self._update_reason = None

    def get_equipment_test(self) -> EquipmentTest:
        """Отдать тест оборудования"""
        return self._equipment_test

    def get_name(self) -> str:
        """Отдать название"""
        return self._name

    def get_data(self) -> str:
        """Отдать данные"""
        return self._data

    def get_employee_entered_data(self) -> Employee:
        """Отдать сотрудника который ввёл данные"""
        return self._employee_entered_data

    def get_employee_updated_data(self) -> Employee:
        """Отдать сотрудника который обновил данные"""
        return self._employee_updated_data

    def get_update_reason(self) -> str:
        """Отдать причину обновления данных"""
        return self._update_reason

    def get_state(self) -> bool:
        """Отдать статус"""
        return self._state

    def update(self, data: str, employee_updated_data: Employee, update_reason: str, state: bool = None):
        """Обновить данные с указанием причины - данные, сотрудник который ввел данные, причина"""
        if not state:  # если не передали state установим его в текущее состояние
            state = self._state

        # проверка
        if not data:  # пустая data недопустима
            return None
        if not isinstance(employee_updated_data, Employee):  # employee_updated_data должен быть Employee
            return None
        if not update_reason:  # пустой update_reason недопустим
            return None
        if not isinstance(state, bool):  # state должен быть bool
            return None

        # обновление
        self._data = data
        self._state = state
        self._employee_updated_data = employee_updated_data
        self._update_reason = update_reason
        return self


class MaintenanceRequest(object):
    """
    Информация о заявке на ТО
    """

    def __new__(cls, uid: int, equipment: Equipment, brigade: Brigade, maintenance_type: MaintenanceType):
        """Инициализация класса результат теста с проверкой данных"""
        if not isinstance(uid, int):
            return None  # не числовой uid недопустим
        if not isinstance(equipment, Equipment):
            return None  # equipment должен быть Equipment
        if not isinstance(brigade, Brigade):
            return None  # brigade должен быть Brigade
        if maintenance_type not in MaintenanceType:
            return None  # невалидный maintenance_type недопустим
        # инициализируем
        instance = super(MaintenanceRequest, cls).__new__(cls)
        return instance

    def __init__(self, uid: int, equipment: Equipment, brigade: Brigade, maintenance_type: MaintenanceType):
        """Новая заявка на ТО - уникальный идентификатор заявки, оборудование, бригада, тип заявки"""
        self._uid = uid
        self._equipment = equipment
        self._brigade = brigade
        self._maintenance_type = maintenance_type

        self._opening_date = datetime.now()
        self._maintenance_request_state = MaintenanceRequestState.is_open

        self._test_result_list = list()
        self._date_of_completion_of_service = None
        self._closing_date = None

    def get_uid(self) -> int:
        """Получить идентификатор"""
        return self._uid

    def get_equipment(self) -> Equipment:
        """Получить оборудование"""
        return self._equipment

    def get_brigade(self) -> Brigade:
        """Получить бригаду"""
        return self._brigade

    def get_maintenance_type(self) -> MaintenanceType:
        """Получить тип заявки"""
        return self._maintenance_type

    def get_maintenance_request_state(self) -> MaintenanceRequestState:
        """Получить статус"""
        return self._maintenance_request_state

    def get_test_result_list(self) -> List:
        """Получить список результатов тестов"""
        return self._test_result_list

    def get_opening_date(self) -> datetime:
        """Получить дату открытия"""
        return self._opening_date

    def get_date_of_completion_of_service(self) -> datetime:
        """Получить дату завершения обслуживания"""
        return self._date_of_completion_of_service

    def get_closing_date(self) -> datetime:
        """Получить дату закрытия"""
        return self._closing_date

    def get_test_result_by_name(self, name: str):
        """Получить результаты теста по его имени"""
        for test in self._test_result_list:
            if test.get_name() == name:
                return test
        return None  # тест не найден

    def get_test_result_by_equipment_test(self, equipment_test: EquipmentTest):
        """Получить результаты теста по тесту оборудования"""
        for test in self._test_result_list:
            if test.get_equipment_test() == equipment_test:
                return test
        return None  # тест не найден

    def _check_can_update_request(self) -> bool:
        """Проверка допустимости изменения заявки"""
        if self._maintenance_request_state == MaintenanceRequestState.is_closed:
            return False
        return True

    def _close_maintenance_request(self):
        """Закрытие заявки"""
        self._closing_date = datetime.now()
        self._maintenance_request_state = MaintenanceRequestState.is_closed

    def add_test_result(self, equipment_test: EquipmentTest, data: str, employee_entered_data: Employee, state: bool):
        """Добавить результаты теста - тест оборудования, данные, кто добавил, статус теста (успешен / нет)"""
        if self._check_can_update_request():
            if not isinstance(equipment_test, EquipmentTest):
                return False  # невалидный тест оборудования

            test = self._equipment.get_test_by_name(name=equipment_test.get_name())
            if not test:
                return False  # тест не найден в списке тестов оборудования

            test = self.get_test_result_by_equipment_test(equipment_test=equipment_test)
            if test:
                return False  # тест найден, нельзя добавлять несколько тестов с одним именем

            test = RequestTestResult(equipment_test=equipment_test, data=data, employee_entered_data=employee_entered_data, state=state)
            if not test:
                return None  # если не смогли инициализировать результат теста

            self._test_result_list.append(test)
            return test
        return False  # запрешено

    def update_test_result(self, equipment_test: EquipmentTest, data: str, employee_updated_data: Employee, update_reason: str, state: bool = None):
        """Обновить результаты теста - тест оборудования, данные, кто обновил, причина, статус теста (успешен / нет) (необязательно)"""
        if self._check_can_update_request():
            test = self.get_test_result_by_equipment_test(equipment_test=equipment_test)
            if not test:
                return None  # тест не найден

            test = test.update(data=data, employee_updated_data=employee_updated_data, update_reason=update_reason, state=state)
            return test  # вернет None если данные были кривые
        return False  # запрешено

    def service_complete(self) -> bool:
        """Завершить обслуживание"""
        if self._date_of_completion_of_service is None:
            self._date_of_completion_of_service = datetime.now()
            self._maintenance_request_state = MaintenanceRequestState.service_completed
            return True
        return False  # запрешено

    def service_data_correct(self) -> bool:
        """Подтвердить корректность"""
        if self._closing_date is None and self._date_of_completion_of_service is not None:
            self._close_maintenance_request()
            return True
        return False  # запрешено
