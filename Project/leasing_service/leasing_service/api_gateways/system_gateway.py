# coding=utf-8
"""
Данный модуль содержит методы реализующие взаимодействие компонентов текущей системы
"""

import time
from datetime import datetime
# Подключение модулей для использования явной типизации
from typing import List

# Подключение модулей для использования явной типизации
from leasing_service.business_classes.equipment_accounting_system_classes import Equipment, EquipmentTest
from leasing_service.business_classes.personnel_accounting_system_classes import Brigade, Employee
# Для работы с заявками
from leasing_service.business_classes.system_classes import MaintenanceRequest, MaintenanceType


class SystemGatewayInMemory(object):
    """
    Класс для обеспечения взаимодействия компонентов текущей системы
    """
    instance = None  # реализация singleton

    def __new__(cls):
        """Инициализация класса"""
        if cls.instance:  # реализация singleton
            return cls.instance

        # инициализируем
        cls.instance = super(SystemGatewayInMemory, cls).__new__(cls)  # реализация singleton
        return cls.instance

    def __init__(self):
        """Инициализация класса"""
        self._maintenance_request_list = []
        self._maintenance_request_last_uid = 0

    @staticmethod
    def _select_maintenance_type(equipment: Equipment) -> MaintenanceType:
        """Принятие решения о типе ТО"""
        # если месяц и день ввода в эксплуатацию равен текущему месяцу и дню, то ТО расширенное
        today = datetime.now()
        com_date = equipment.get_commissioning_date()
        if today.month == com_date.month and today.day == com_date.day and today.year != com_date.year:
            return MaintenanceType.advanced
        return MaintenanceType.simple

    def clear_maintenance_request_list(self):
        """Очистка списка заявок на ТО"""
        self._maintenance_request_list = []
        self._maintenance_request_last_uid = 0

    def get_maintenance_requests(self) -> List:
        """Возвращает список заявок на ТО"""
        return self._maintenance_request_list

    def get_maintenance_request_by_uid(self, uid: int):
        """Возвращает заявку на ТО по её uid"""
        for maintenance_request in self._maintenance_request_list:
            if maintenance_request.get_uid() == uid:
                return maintenance_request
        raise ValueError("Заявка не найдена")

    def add_maintenance_request(self, equipment: Equipment, brigade: Brigade) -> int:
        """Добавить заявку на ТО в список заявок"""
        # Определяем тип ТО
        maintenance_type = self._select_maintenance_type(equipment)

        # Формируем uid
        self._maintenance_request_last_uid += 1
        uid = self._maintenance_request_last_uid

        # Формируем и добавляем заявку
        maintenance_request = MaintenanceRequest(
                uid=uid,
                equipment=equipment,
                brigade=brigade,
                maintenance_type=maintenance_type)
        self._maintenance_request_list.append(maintenance_request)

        # возвращаем uid заявки
        return uid

    def add_maintenance_request_test_result_by_uid(self, uid: int, equipment_test: EquipmentTest,
                                                   data: str, employee: Employee, state: bool):
        """Добавляет тест к заявке или бросает исключение"""
        # Получение заявки по её номеру
        maintenance_request = self.get_maintenance_request_by_uid(uid)

        # Добавить к заявке результат теста
        return_value = maintenance_request.add_test_result(
            equipment_test=equipment_test,
            data=data,
            employee_entered_data=employee,
            state=state
        )

        # Если при добавлении вернулось False или None - ошибка
        if not return_value:
            raise ValueError("Результаты теста не были добавлены")

    def update_maintenance_request_test_result_by_uid(self, uid: int, equipment_test: EquipmentTest, data: str,
                                                      employee: Employee, update_reason: str, state: bool = None):
        """Обновляет тест в заявке или бросает исключение"""
        # Получение заявки по её номеру
        maintenance_request = self.get_maintenance_request_by_uid(uid)

        # Обновить в заявке результат теста
        return_value = maintenance_request.update_test_result(
            equipment_test=equipment_test,
            data=data,
            employee_updated_data=employee,
            update_reason=update_reason,
            state=state
        )

        # Если при обновлении вернулось False или None - ошибка
        if not return_value:
            raise ValueError("Результаты теста не были обновлены")

    def maintenance_request_service_complete_by_uid(self, uid: int):
        """Завершает обслуживание заявки или бросает исключение"""
        # Получение заявки по её номеру
        maintenance_request = self.get_maintenance_request_by_uid(uid)

        # Завершить ТО у заявки
        return_value = maintenance_request.service_complete()

        # Если при завершении вернулось False - ошибка
        if not return_value:
            raise ValueError("У заявки уже завершено ТО")

    def maintenance_request_service_data_correct_by_uid(self, uid: int):
        """Подтверждает корректность заявки или бросает исключение"""
        # Получение заявки по её номеру
        maintenance_request = self.get_maintenance_request_by_uid(uid)

        # Подтверждение корректности заявки
        return_value = maintenance_request.service_data_correct()

        # Если при подтверждении корректности вернулось False - ошибка
        if not return_value:
            raise ValueError("Заявка уже подтверждена или ТО еще не завершено")

    def get_last_equipment_maintenance_request(self, equipment: Equipment) -> MaintenanceRequest:
        """Возвращает последнюю заявку на ТО для оборудования или бросает исключение"""
        """
        1. Выбор последней заявки на ТО для переданного оборудования
        1.1. Проверка что заявка закрыта и оборудования тоже что и передали
        2. Если заявка на ТО для оборудования не найдена - ошибка
        3. Возврат найденной заявки
        """
        # Выбор последней заявки на ТО для переданного оборудования
        max_timestamp = 0
        last_equipment_maintenance_request = None
        for maintenance_request in self._maintenance_request_list:
            # Проверка что заявка закрыта и оборудования тоже что и передали
            close_date = maintenance_request.get_closing_date()
            if close_date and maintenance_request.get_equipment() == equipment:
                maintenance_request_timestamp = time.mktime(close_date.timetuple())
                if maintenance_request_timestamp == max_timestamp:  # если равны задержки берем последнее по uid ТО
                    if last_equipment_maintenance_request.get_uid() < maintenance_request.get_uid():
                        last_equipment_maintenance_request = maintenance_request
                elif maintenance_request_timestamp > max_timestamp:
                    max_timestamp = maintenance_request_timestamp
                    last_equipment_maintenance_request = maintenance_request

        # Если заявка на ТО для оборудования не найдена - ошибка
        if not last_equipment_maintenance_request:
            raise ValueError("Для выбранного оборудования не найдено ни одной заявки на ТО")

        # Возврат найденной заявки
        return last_equipment_maintenance_request
