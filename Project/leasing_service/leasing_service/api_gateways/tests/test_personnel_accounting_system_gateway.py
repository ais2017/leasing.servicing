# coding=utf-8
from unittest import mock

import pytest

# Тестируемый шлюз
from leasing_service.api_gateways.personnel_accounting_system_gateway import PersonnelAccountingSystemGateway
# Дополнительные классы
from leasing_service.business_classes.personnel_accounting_system_classes import Employee, Brigade

"""
Чек-лист класса PersonnelAccountingSystemGateway:
    + 1. Проверка что класс шлюза PersonnelAccountingSystemGateway действительно singleton
    + 2. Проверка что данные о бригадах успешно очищаются
    + 3. Проверка что данные о бригадах успешно получаются списком
    + 4. Сотрудник не найден по uid
"""

# сотрудники
employees = [
    Employee(uid=0, name='Василий'),
    Employee(uid=1, name='Петр'),
    Employee(uid=2, name='Елизавета'),
    Employee(uid=3, name='Артем'),
    Employee(uid=4, name='Евгений'),
    Employee(uid=5, name='Александр'),
]

# список бригад
brigades = [
    Brigade(uid=0, employee_list=employees[0:3], brigadier=employees[0]),
    Brigade(uid=1, employee_list=employees[3:6], brigadier=employees[3]),
]

# инстанс класса шлюза
pa_system_gateway = PersonnelAccountingSystemGateway()


# тесты PersonnelAccountingSystemGateway
@pytest.mark.PersonnelAccountingSystemGateway
def test_personnel_asg_singleton():
    """Проверка что класс шлюза PersonnelAccountingSystemGateway действительно singleton"""
    pa_system_gateway_second = PersonnelAccountingSystemGateway()
    assert pa_system_gateway == pa_system_gateway_second


@pytest.mark.PersonnelAccountingSystemGateway
def test_personnel_asg_clear_equipment_list():
    """Проверка что данные о бригадах успешно очищаются"""
    with mock.patch.object(PersonnelAccountingSystemGateway, '_request_to_system') as mocked_request_to_system:
        mocked_request_to_system.side_effect = None
        mocked_request_to_system.return_value = {'brigades': brigades, 'employees': employees}
        assert pa_system_gateway.get_brigades()
        assert len(pa_system_gateway.get_brigades()) > 0
    pa_system_gateway.clear_brigades_and_employees_list()
    assert pa_system_gateway.get_brigades() is None


@pytest.mark.PersonnelAccountingSystemGateway
def test_personnel_asg_get_all():
    """Проверка что данные о бригадах успешно получаются списком"""
    with mock.patch.object(PersonnelAccountingSystemGateway, '_request_to_system') as mocked_request_to_system:
        mocked_request_to_system.side_effect = None
        mocked_request_to_system.return_value = {'brigades': brigades, 'employees': employees}
        brigades_list = pa_system_gateway.get_brigades()
    assert pa_system_gateway.get_brigades()
    assert len(brigades_list) > 0
    assert brigades[0] in brigades_list
    assert brigades[1] in brigades_list


@pytest.mark.SystemGateway
def test_personnel_asg_fail_get_employee_by_uid():
    """Сотрудник не найден по uid"""
    with pytest.raises(ValueError, match="Сотрудник не найден"):
        pa_system_gateway.get_employee_by_uid(-100)
