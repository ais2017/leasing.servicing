# coding=utf-8
from datetime import datetime, timedelta
from unittest import mock

import pytest

# Тестируемый шлюз
from leasing_service.api_gateways.equipment_accounting_system_gateway import EquipmentAccountingSystemGateway
# Дополнительные классы
from leasing_service.business_classes.equipment_accounting_system_classes \
    import MaintenanceType, EquipmentTest, Equipment

"""
Чек-лист класса EquipmentAccountingSystemGateway:
    + 1. Проверка что класс шлюза EquipmentAccountingSystemGateway действительно singleton
    + 2. Проверка что данные об оборудовании успешно очищаются
    + 3. Проверка что данные об оборудовании успешно получаются списком
    + 4. Оборудование не найдено по uid
"""

# Тесты оборудования
equipment_tests = [
    EquipmentTest(name='Проверка диафрагмы', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Чистка temp файлов', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Тестовый запуск', maintenance_type=MaintenanceType.advanced),
    EquipmentTest(name='UNKNOWN', maintenance_type=MaintenanceType.advanced),
]

# даты
commissioning_dates = [
    datetime.now(),
    datetime.now() - timedelta(days=1),
    datetime.now() - timedelta(days=365),
]

# список оборудования
equipments = [
    Equipment(uid=0, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[0]),
    Equipment(uid=1, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[1]),
    Equipment(uid=2, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[2]),
]

# инстанс класса шлюза
ea_system_gateway = EquipmentAccountingSystemGateway()


# тесты EquipmentAccountingSystemGateway
@pytest.mark.EquipmentAccountingSystemGateway
def test_equipment_asg_singleton():
    """Проверка что класс шлюза EquipmentAccountingSystemGateway действительно singleton"""
    ea_system_gateway_second = EquipmentAccountingSystemGateway()
    assert ea_system_gateway == ea_system_gateway_second


@pytest.mark.EquipmentAccountingSystemGateway
def test_equipment_asg_clear_equipment_list():
    """Проверка что данные об оборудовании успешно очищаются"""
    with mock.patch.object(EquipmentAccountingSystemGateway, '_request_to_system') as mocked_request_to_system:
        mocked_request_to_system.side_effect = None
        mocked_request_to_system.return_value = {'equipments': equipments, 'equipment_tests': equipment_tests}
        assert ea_system_gateway.get_equipments()
        assert len(ea_system_gateway.get_equipments()) > 0
    ea_system_gateway.clear_equipments_tests_and_equipments_list()
    assert ea_system_gateway.get_equipments() is None


@pytest.mark.EquipmentAccountingSystemGateway
def test_equipment_asg_get_all():
    """Проверка что данные об оборудовании успешно получаются списком"""
    with mock.patch.object(EquipmentAccountingSystemGateway, '_request_to_system') as mocked_request_to_system:
        mocked_request_to_system.side_effect = None
        mocked_request_to_system.return_value = {'equipments': equipments, 'equipment_tests': equipment_tests}
        equipments_list = ea_system_gateway.get_equipments()
    assert ea_system_gateway.get_equipments()
    assert len(equipments_list) > 0
    assert equipments[0] in equipments_list
    assert equipments[1] in equipments_list
    assert equipments[2] in equipments_list


@pytest.mark.SystemGateway
def test_equipment_asg_fail_get_equipment_by_uid():
    """Оборудование не найдено по uid"""
    with pytest.raises(ValueError, match="Оборудование не найдено"):
        ea_system_gateway.get_equipment_by_uid(-100)
