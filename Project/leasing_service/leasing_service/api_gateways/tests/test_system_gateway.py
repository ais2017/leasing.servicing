# coding=utf-8
from datetime import datetime, timedelta

import pytest

# Тестируемый шлюз
from leasing_service.api_gateways.system_gateway import SystemGatewayInMemory
# дополнительные классы
from leasing_service.business_classes.equipment_accounting_system_classes \
    import MaintenanceType, EquipmentTest, Equipment
from leasing_service.business_classes.personnel_accounting_system_classes import Employee, Brigade
from leasing_service.business_classes.system_classes import MaintenanceRequestState

"""
Чек-лист класса SystemGatewayInMemory:
    Позитивные тесты:
        + 1. Проверка что класс шлюза SystemGatewayInMemory действительно singleton
        + 2. Проверка что данные о новой заявке успешно добавляются и получаются по uid
        + 3. Проверка что данные о заявках успешно очищаются
        + 4. Проверка что данные о заявке успешно получаются списком
        + 5. Проверка что результат теста успешно добавляется к заявке
        + 6. Проверка что результат теста успешно обновляется в заявке
        + 7. Проверка что у заявки успешно завершается обслуживание
        + 8. Проверка что заявка успешно закрывается
        + 9. Проверка что корректно выбирается тип заявки на ТО
        + 10. Проверка успешности получения последней заявки для оборудования

    Негативные тесты:
        + 1. Проверка что кривой результат теста не добавляется к заявке
        + 2. Проверка что кривой результат теста не обновляется в заявке
        + 3. Проверка что у заявки не завершается обслуживание
        + 4. Проверка что заявка не закрывается
        + 5. Проверка неудачи в получении последней заявки для оборудования
        + 6. Заявка не найдена по uid
"""

# сотрудники
employees = [
    Employee(uid=0, name='Василий'),
    Employee(uid=1, name='Петр'),
    Employee(uid=2, name='Елизавета'),
    Employee(uid=3, name='Артем'),
    Employee(uid=4, name='Евгений'),
    Employee(uid=5, name='Александр'),
]

# список бригад
brigades = [
    Brigade(uid=0, employee_list=employees[0:3], brigadier=employees[0]),
    Brigade(uid=1, employee_list=employees[3:6], brigadier=employees[3]),
]

# Тесты оборудования
equipment_tests = [
    EquipmentTest(name='Проверка диафрагмы', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Чистка temp файлов', maintenance_type=MaintenanceType.simple),
    EquipmentTest(name='Тестовый запуск', maintenance_type=MaintenanceType.advanced),
    EquipmentTest(name='UNKNOWN', maintenance_type=MaintenanceType.advanced),
]

# даты
commissioning_dates = [
    datetime.now(),
    datetime.now() - timedelta(days=1),
    datetime.now() - timedelta(days=365),
]

# список оборудования
equipments = [
    Equipment(uid=0, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[0]),
    Equipment(uid=1, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[1]),
    Equipment(uid=2, equipment_test_list=equipment_tests[0:3], commissioning_date=commissioning_dates[2]),
]

# инстанс класса шлюза
system_gateway = SystemGatewayInMemory()


# тесты SystemGatewayInMemory
# Позитивные тесты
@pytest.mark.SystemGateway
def test_system_gim_singleton():
    """Проверка что класс шлюза SystemGatewayInMemory действительно singleton"""
    system_gateway_second = SystemGatewayInMemory()
    assert system_gateway == system_gateway_second


@pytest.mark.SystemGateway
def test_system_gim_add_and_get_mr():
    """Проверка что данные о новой заявке успешно добавляются и получаются по uid"""
    uid = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    assert system_gateway.get_maintenance_request_by_uid(uid).get_uid() == uid
    assert system_gateway.get_maintenance_request_by_uid(uid).get_equipment() == equipments[0]
    assert system_gateway.get_maintenance_request_by_uid(uid).get_brigade() == brigades[0]


@pytest.mark.SystemGateway
def test_system_gim_clear_mr_list():
    """Проверка что данные о заявках успешно очищаются"""
    system_gateway.add_maintenance_request(equipments[1], brigades[1])
    assert len(system_gateway.get_maintenance_requests()) > 0
    system_gateway.clear_maintenance_request_list()
    assert len(system_gateway.get_maintenance_requests()) == 0


@pytest.mark.SystemGateway
def test_system_gim_get_all():
    """Проверка что данные о заявке успешно получаются списком"""
    uid1 = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    uid2 = system_gateway.add_maintenance_request(equipments[1], brigades[1])
    mr1 = system_gateway.get_maintenance_request_by_uid(uid1)
    mr2 = system_gateway.get_maintenance_request_by_uid(uid2)
    mr_list = system_gateway.get_maintenance_requests()
    assert mr1 in mr_list
    assert mr2 in mr_list


@pytest.mark.SystemGateway
def test_system_gim_success_add_tr_to_mr():
    """Проверка что результат теста успешно добавляется к заявке"""
    uid = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    data = 'test_data'
    state = True
    system_gateway.add_maintenance_request_test_result_by_uid(uid, equipment_tests[0], data, employees[0], state)
    mr = system_gateway.get_maintenance_request_by_uid(uid)
    assert mr.get_uid() == uid
    assert mr.get_equipment() == equipments[0]
    assert mr.get_brigade() == brigades[0]
    assert mr.get_maintenance_type() == MaintenanceType.simple
    assert mr.get_maintenance_request_state() == MaintenanceRequestState.is_open
    assert mr.get_test_result_by_name(equipment_tests[0].get_name()).get_name() == equipment_tests[0].get_name()


@pytest.mark.SystemGateway
def test_system_gim_success_upd_tr_in_mr():
    """Проверка что результат теста успешно обновляется в заявке"""
    uid = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    data = 'test_data'
    state = True
    system_gateway.add_maintenance_request_test_result_by_uid(uid, equipment_tests[0], data, employees[0], state)
    mr = system_gateway.get_maintenance_request_by_uid(uid)
    mr_tr = mr.get_test_result_by_name(equipment_tests[0].get_name())
    assert mr_tr.get_data() == data

    # обновление
    new_data = 'test_data'
    update_reason = 'because I want'
    system_gateway.update_maintenance_request_test_result_by_uid(uid, equipment_tests[0], new_data, employees[1], update_reason)
    mr_tr_updated = mr.get_test_result_by_name(equipment_tests[0].get_name())
    assert mr_tr_updated == mr_tr
    assert mr_tr_updated.get_data() == new_data
    assert mr_tr_updated.get_update_reason() == update_reason
    assert mr_tr_updated.get_employee_updated_data() == employees[1]


@pytest.mark.SystemGateway
def test_system_gim_success_mr_service_complete():
    """Проверка что у заявки успешно завершается обслуживание"""
    uid = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(uid)
    assert mr.get_maintenance_request_state() == MaintenanceRequestState.is_open
    system_gateway.maintenance_request_service_complete_by_uid(uid)
    assert mr.get_maintenance_request_state() == MaintenanceRequestState.service_completed


@pytest.mark.SystemGateway
def test_system_gim_success_mr_service_data_correct():
    """Проверка что заявка успешно закрывается"""
    uid = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(uid)
    assert mr.get_maintenance_request_state() == MaintenanceRequestState.is_open
    system_gateway.maintenance_request_service_complete_by_uid(uid)
    assert mr.get_maintenance_request_state() == MaintenanceRequestState.service_completed
    system_gateway.maintenance_request_service_data_correct_by_uid(uid)
    assert mr.get_maintenance_request_state() == MaintenanceRequestState.is_closed


@pytest.mark.SystemGateway
def test_system_gim_check_mr_type():
    """Проверка что корректно выбирается тип заявки на ТО"""
    # сегодняшнее оборудование
    uid1 = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr1 = system_gateway.get_maintenance_request_by_uid(uid1)
    assert mr1.get_maintenance_type() == MaintenanceType.simple

    # вчерашнее оборудование
    uid2 = system_gateway.add_maintenance_request(equipments[1], brigades[0])
    mr2 = system_gateway.get_maintenance_request_by_uid(uid2)
    assert mr2.get_maintenance_type() == MaintenanceType.simple

    # оборудованию год
    uid3 = system_gateway.add_maintenance_request(equipments[2], brigades[0])
    mr3 = system_gateway.get_maintenance_request_by_uid(uid3)
    assert mr3.get_maintenance_type() == MaintenanceType.advanced


@pytest.mark.SystemGateway
def test_system_gim_success_get_last_equipment_mr_test():
    """Проверка успешности получения последней заявки для оборудования"""
    uid1 = system_gateway.add_maintenance_request(equipments[2], brigades[0])
    system_gateway.maintenance_request_service_complete_by_uid(uid1)
    system_gateway.maintenance_request_service_data_correct_by_uid(uid1)
    assert system_gateway.get_last_equipment_maintenance_request(equipments[2]).get_uid() == uid1

    uid2 = system_gateway.add_maintenance_request(equipments[2], brigades[0])
    system_gateway.maintenance_request_service_complete_by_uid(uid2)
    system_gateway.maintenance_request_service_data_correct_by_uid(uid2)
    assert system_gateway.get_last_equipment_maintenance_request(equipments[2]).get_uid() == uid2


# Негативные тесты
@pytest.mark.SystemGateway
def test_system_gim_fail_add_tr_to_mr():
    """Проверка что кривой результат теста не добавляется к заявке"""
    uid = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    data = 'test_data'
    state = None

    # невозможно добавить тест без статуса
    with pytest.raises(ValueError, match="Результаты теста не были добавлены"):
        system_gateway.add_maintenance_request_test_result_by_uid(uid, equipment_tests[0], data, employees[0], state)


@pytest.mark.SystemGateway
def test_system_gim_fail_upd_tr_in_mr():
    """Проверка что кривой результат теста не обновляется в заявке"""
    uid = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    data = 'test_data'
    state = True
    system_gateway.add_maintenance_request_test_result_by_uid(uid, equipment_tests[0], data, employees[0], state)
    mr = system_gateway.get_maintenance_request_by_uid(uid)
    mr_tr = mr.get_test_result_by_name(equipment_tests[0].get_name())
    assert mr_tr.get_data() == data

    # обновление без указания причины обновления
    new_data = 'test_data'
    update_reason = ''
    with pytest.raises(ValueError, match="Результаты теста не были обновлены"):
        system_gateway.update_maintenance_request_test_result_by_uid(uid, equipment_tests[0], new_data, employees[1], update_reason)
    mr_tr_updated = mr.get_test_result_by_name(equipment_tests[0].get_name())
    assert mr_tr_updated == mr_tr
    assert mr_tr_updated.get_data() == data
    assert mr_tr_updated.get_update_reason() is None
    assert mr_tr_updated.get_employee_updated_data() is None


@pytest.mark.SystemGateway
def test_system_gim_fail_mr_service_complete():
    """Проверка что у заявки не завершается обслуживание"""
    uid = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(uid)
    assert mr.get_maintenance_request_state() == MaintenanceRequestState.is_open

    system_gateway.maintenance_request_service_complete_by_uid(uid)
    assert mr.get_maintenance_request_state() == MaintenanceRequestState.service_completed

    # Двойное завершение ТО (вторая попытка не проходит)
    with pytest.raises(ValueError, match="У заявки уже завершено ТО"):
        system_gateway.maintenance_request_service_complete_by_uid(uid)


@pytest.mark.SystemGateway
def test_system_gim_fail_mr_service_data_correct():
    """Проверка что заявка не закрывается"""
    uid = system_gateway.add_maintenance_request(equipments[0], brigades[0])
    mr = system_gateway.get_maintenance_request_by_uid(uid)
    assert mr.get_maintenance_request_state() == MaintenanceRequestState.is_open

    # попытка закрытия заявки до завершения ТО
    with pytest.raises(ValueError, match="Заявка уже подтверждена или ТО еще не завершено"):
        system_gateway.maintenance_request_service_data_correct_by_uid(uid)


@pytest.mark.SystemGateway
def test_system_gim_fail_get_last_equipment_mr_test():
    """Проверка неудачи в получении последней заявки для оборудования"""
    with pytest.raises(ValueError, match="Для выбранного оборудования не найдено ни одной заявки на ТО"):
        system_gateway.get_last_equipment_maintenance_request(equipments[1]).get_uid()


@pytest.mark.SystemGateway
def test_system_gim_fail_get_mr_by_uid():
    """Заявка не найдена по uid"""
    with pytest.raises(ValueError, match="Заявка не найдена"):
        system_gateway.get_maintenance_request_by_uid(-100)
