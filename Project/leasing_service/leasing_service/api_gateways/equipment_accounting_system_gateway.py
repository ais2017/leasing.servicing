# coding=utf-8
"""
Данный модуль содержит методы реализующие взаимодействие с системой учета оборудования
"""


class EquipmentAccountingSystemGateway(object):
    """
    Класс для обеспечения взаимодействия с системой учета оборудования
    """
    instance = None  # реализация singleton

    def __new__(cls):
        """Инициализация класса"""
        if cls.instance:  # реализация singleton
            return cls.instance

        # инициализируем
        cls.instance = super(EquipmentAccountingSystemGateway, cls).__new__(cls)  # реализация singleton
        return cls.instance

    def __init__(self):
        """Инициализация класса"""
        self._tests_list = []
        self._equipments_list = []

    def clear_equipments_tests_and_equipments_list(self):
        """Очистка списка сотрудников и бригад"""
        self._tests_list = []
        self._equipments_list = []

    def _request_to_system(self):
        """Запрос данных из внешней системы"""
        # ToDo: заглушка - запрос данных из внешней системы
        return {}

    def _form_classes(self, data):
        """Заполнение данными списков тестов оборудования и оборудования"""
        # ToDo: заглушка - упаковка данных из внешней системы в классы используемые текущей системой
        self._tests_list = data.get('equipment_tests')
        self._equipments_list = data.get('equipments')

    def get_equipments(self):
        """Вернуть список оборудования с соответствующими тестами"""
        if self._equipments_list:
            return self._equipments_list
        self._form_classes(self._request_to_system())  # формирование списка тестов оборудования и оборудования
        return self._equipments_list

    def get_equipment_by_uid(self, uid: int):
        for equipment in self.get_equipments():
            if equipment.get_uid() == uid:
                return equipment
        raise ValueError("Оборудование не найдено")
