# coding=utf-8
"""
Данный модуль содержит методы реализующие взаимодействие с системой учета персонала
"""


class PersonnelAccountingSystemGateway(object):
    """
    Класс для обеспечения взаимодействия с системой учета персонала
    """
    instance = None  # реализация singleton

    def __new__(cls):
        """Инициализация класса"""
        if cls.instance:  # реализация singletons
            return cls.instance

        # инициализируем
        cls.instance = super(PersonnelAccountingSystemGateway, cls).__new__(cls)  # реализация singleton
        return cls.instance

    def __init__(self):
        """Инициализация класса"""
        self._employees_list = []
        self._brigades_list = []

    def clear_brigades_and_employees_list(self):
        """Очистка списка сотрудников и бригад"""
        self._employees_list = []
        self._brigades_list = []

    def _request_to_system(self):
        """Запрос данных из внешней системы"""
        # ToDo: заглушка - запрос данных из внешней системы
        return {}

    def _form_classes(self, data):
        """Заполнение данными списков бригад и сотрудников"""
        # ToDo: заглушка - упаковка данных из внешней системы в классы используемые текущей системой
        self._employees_list = data.get('employees')
        self._brigades_list = data.get('brigades')

    def get_brigades(self):
        """Вернуть список бригад с входящими в них сотрудниками"""
        if self._brigades_list:
            return self._brigades_list
        self._form_classes(self._request_to_system())  # формирование списка бригад и сотрудников
        return self._brigades_list

    def get_employee_by_uid(self, uid: int):
        """Вернуть сотруника по его uid"""
        self.get_brigades()
        for employee in self._employees_list:
            if employee.get_uid() == uid:
                return employee
        raise ValueError("Сотрудник не найден")

    def authentificate(self, request, username: str, password: str):
        # ToDo: запрос данных о сотруднике и получение информации о нём (из системы учета персонала)
        position = 'engineer'  # заглушка
        if len(password) > 10:
            position = 'brigadier'
        return {'uid': 1, 'brigade_uid': 1, 'username': username, 'position': position}
